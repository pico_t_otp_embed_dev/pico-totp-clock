use embassy_sync::blocking_mutex::raw::ThreadModeRawMutex;
use embassy_sync::mutex::Mutex;
use embassy_time::{Duration, Timer};

use embassy_rp::dma::{AnyChannel, Channel};
use embassy_rp::peripherals::PIO0;
use embassy_rp::pio::{
    Common, Config, FifoJoin, Instance, PioPin, ShiftConfig, ShiftDirection, StateMachine,
};
use embassy_rp::{clocks, into_ref, Peripheral, PeripheralRef};

use fixed::types::U24F8;
use fixed_macro::fixed;
use smart_leds::{RGB, RGB8};

pub type Ws2812_1 = Ws2812<'static, PIO0, 3, 1>; // For a device of one Ws2812

pub struct Ws2812<'d, P: Instance, const S: usize, const N: usize> {
    dma: PeripheralRef<'d, AnyChannel>,
    sm: StateMachine<'d, P, S>,
}

impl<'d, P: Instance, const S: usize, const N: usize> Ws2812<'d, P, S, N> {
    pub fn new(
        pio: &mut Common<'d, P>,
        mut sm: StateMachine<'d, P, S>,
        dma: impl Peripheral<P = impl Channel> + 'd,
        pin: impl PioPin,
    ) -> Self {
        into_ref!(dma);

        // prepare the PIO program
        let side_set = pio::SideSet::new(false, 1, false);
        let mut a: pio::Assembler<32> = pio::Assembler::new_with_side_set(side_set);

        const T1: u8 = 2; // start bit
        const T2: u8 = 5; // data bit
        const T3: u8 = 3; // stop bit
        const CYCLES_PER_BIT: u32 = (T1 + T2 + T3) as u32;

        let mut wrap_target = a.label();
        let mut wrap_source = a.label();
        let mut do_zero = a.label();
        a.set_with_side_set(pio::SetDestination::PINDIRS, 1, 0);
        a.bind(&mut wrap_target);
        // Do stop bit
        a.out_with_delay_and_side_set(pio::OutDestination::X, 1, T3 - 1, 0);
        // Do start bit
        a.jmp_with_delay_and_side_set(pio::JmpCondition::XIsZero, &mut do_zero, T1 - 1, 1);
        // Do data bit = 1
        a.jmp_with_delay_and_side_set(pio::JmpCondition::Always, &mut wrap_target, T2 - 1, 1);
        a.bind(&mut do_zero);
        // Do data bit = 0
        a.nop_with_delay_and_side_set(T2 - 1, 0);
        a.bind(&mut wrap_source);

        let prg = a.assemble_with_wrap(wrap_source, wrap_target);

        let mut cfg = Config::default();

        // Pin config
        let out_pin = pio.make_pio_pin(pin);
        cfg.set_out_pins(&[&out_pin]);
        cfg.set_set_pins(&[&out_pin]);

        cfg.use_program(&pio.load_program(&prg), &[&out_pin]);

        // Clock config, measured in kHz to avoid overflows
        // TODO CLOCK_FREQ should come from embassy_rp
        let clock_freq = U24F8::from_num(clocks::clk_sys_freq() / 1000);
        let ws2812_freq = fixed!(800: U24F8);
        let bit_freq = ws2812_freq * CYCLES_PER_BIT;
        cfg.clock_divider = clock_freq / bit_freq;

        // FIFO config
        cfg.fifo_join = FifoJoin::TxOnly;
        cfg.shift_out = ShiftConfig {
            auto_fill: true,
            threshold: 24,
            direction: ShiftDirection::Left,
        };

        sm.set_config(&cfg);
        sm.set_enable(true);

        Self {
            dma: dma.map_into(),
            sm,
        }
    }

    pub async fn write(&mut self, colors: &[RGB8; N]) {
        // Precompute the word bytes from the colors
        let mut words = [0u32; N];
        for i in 0..N {
            let word = (u32::from(colors[i].g) << 24)
                | (u32::from(colors[i].r) << 16)
                | (u32::from(colors[i].b) << 8);
            words[i] = word;
        }

        // DMA transfer
        self.sm.tx().dma_push(self.dma.reborrow(), &words).await;

        Timer::after_micros(55).await;
    }
}

#[embassy_executor::task]
pub async fn blink_led_task(led: &'static mut Ws2812_1) {
    led.write(&[LED_COLOR::OFF]).await;
    loop {
        let lb = get_rgb_led().await;
        if let Some(nb_blink) = lb.nb_blink {
            if nb_blink > 0 {
                for _ in 0..nb_blink {
                    led.write(&[lb.color1]).await;
                    Timer::after(lb.speed).await;
                    led.write(&[lb.color2]).await;
                    Timer::after(lb.speed).await;
                }
                Timer::after(lb.delay).await;
            } else {
                led.write(&[lb.color1]).await;
            }
        } else {
            led.write(&[LED_COLOR::OFF]).await;
        }
        Timer::after(Duration::from_millis(50)).await;
    }
}

#[allow(non_camel_case_types)]
pub struct LED_COLOR;
impl LED_COLOR {
    //pub const RED: RGB8 = RGB { r: 86, g: 0, b: 0 };
    pub const RED: RGB8 = RGB { r: 54, g: 0, b: 0 };
    pub const GREEN: RGB8 = RGB { r: 0, g: 54, b: 0 };
    pub const BLUE: RGB8 = RGB { r: 0, g: 0, b: 54 };
    pub const ORANGE: RGB8 = RGB { r: 54, g: 20, b: 0 };
    pub const OFF: RGB8 = RGB { r: 0, g: 0, b: 0 };
}

#[derive(Clone, Copy)]
pub struct RgbLed {
    pub nb_blink: Option<u8>,
    color1: RGB8,
    color2: RGB8,
    speed: Duration,
    delay: Duration,
}

static RGB_LED: Mutex<ThreadModeRawMutex, RgbLed> = Mutex::new(RgbLed {
    nb_blink: None,
    color1: LED_COLOR::OFF,
    color2: LED_COLOR::RED,
    speed: Duration::from_millis(180),
    delay: Duration::from_millis(500),
});

pub async fn blink_rgb_led(nb_blink: u8, color1: RGB8, color2: RGB8) {
    let mut rgb_led = RGB_LED.lock().await;
    rgb_led.nb_blink = Some(nb_blink);
    rgb_led.color1 = color1;
    rgb_led.color2 = color2;
}

pub async fn paint_rgb_led(color: RGB8) {
    let mut rgb_led = RGB_LED.lock().await;
    rgb_led.nb_blink = Some(0);
    rgb_led.color1 = color;
}

pub async fn erase_rgb_led() {
    RGB_LED.lock().await.nb_blink = None;
}

pub async fn get_rgb_led() -> RgbLed {
    *RGB_LED.lock().await
}
