#![no_std]
#![feature(type_alias_impl_trait)]
#![feature(impl_trait_in_assoc_type)]

pub mod display;
pub mod enc;
pub mod nfc;
pub mod rotenc;
pub mod sntp;
pub mod totp_clock;
pub mod usb;
pub mod usb_net;
pub mod wifi;
pub mod ws2812;

use core::str;

use embassy_sync::blocking_mutex::raw::{NoopRawMutex, ThreadModeRawMutex};
use embassy_sync::mutex::Mutex;

use embassy_rp::bind_interrupts;
use embassy_rp::peripherals::PIO0;
use embassy_rp::pio::InterruptHandler as PioInterruptHandler;

use heapless::{String, Vec};

// My own library
use ee24x08::Ee24x08;

bind_interrupts!(pub struct Pio0Irqs {
    PIO0_IRQ_0 => PioInterruptHandler<PIO0>;
});

#[macro_export]
macro_rules! mtx_deref_mut {
    ($mtx:ident) => {
        $mtx.lock().await.deref_mut()
    };
}

#[macro_export]
macro_rules! unsafe_p_clone {
    ($obj:expr) => {
        unsafe { $obj.clone_unchecked() }
    };
}

#[allow(non_camel_case_types)]
pub struct EE_CFG_ADD;
impl EE_CFG_ADD {
    pub const SSID: u32 = 960;
    pub const WIFIPASS: u32 = 976;
    pub const NTP: u32 = 992;
    pub const TAGID: u32 = 1008;
    pub const CC20XN: u32 = 1011;
    pub const TOTP_COUNT: u32 = 1023;
}

#[allow(non_camel_case_types)]
pub struct EE_NET_CFG;
impl EE_NET_CFG {
    pub const LEN: usize = 15;
}

#[derive(Debug)]
pub struct TotpSecretKey {
    pub title: String<14>,
    pub key: Vec<u8, 32>,
}

pub type EeMtx = Mutex<NoopRawMutex, Ee24x08>;

pub const VERSION: &str = env!("CARGO_PKG_VERSION");

pub const CC20_NONCE: &[u8; 12] = b"47120D7246FD"; // 12

static SERIAL_NUMBER: Mutex<ThreadModeRawMutex, [u8; 8]> = Mutex::new([0; 8]);

pub async fn init_serial_number(new_serial: [u8; 8]) {
    *SERIAL_NUMBER.lock().await = new_serial;
}
pub async fn get_serial_number() -> [u8; 8] {
    *SERIAL_NUMBER.lock().await
}

struct NfcTagId {
    id: [u8; 7],
    initialised: bool,
}

static NFC_TAG_ID: Mutex<ThreadModeRawMutex, NfcTagId> = Mutex::new(NfcTagId {
    id: [0; 7],
    initialised: false,
});

pub async fn init_nfc_tag_id(new_nfc_tag_id: [u8; 7]) {
    let mut nfc_tag_id = NFC_TAG_ID.lock().await;
    if !nfc_tag_id.initialised {
        nfc_tag_id.id = new_nfc_tag_id;
        nfc_tag_id.initialised = true;
    }
}
pub async fn get_nfc_tag_id() -> [u8; 7] {
    NFC_TAG_ID.lock().await.id
}

struct Cc20Key {
    key: [u8; 32],
    initialised: bool,
}

static CC20_KEY: Mutex<ThreadModeRawMutex, Cc20Key> = Mutex::new(Cc20Key {
    key: [0; 32],
    initialised: false,
});

pub async fn init_cc20_key(new_key: [u8; 32]) {
    let mut cc20_key = CC20_KEY.lock().await;
    if !cc20_key.initialised {
        cc20_key.key = new_key;
        cc20_key.initialised = true;
    }
}

pub async fn get_cc20_key() -> [u8; 32] {
    CC20_KEY.lock().await.key
}

struct TotpSkList {
    data: Vec<TotpSecretKey, 20>,
    initialised: bool,
}

// Vector of totp secret key
static TOTP_SK_LIST: Mutex<ThreadModeRawMutex, TotpSkList> = Mutex::new(TotpSkList {
    data: Vec::new(),
    initialised: false,
});

// Convert u8 to hexadecimal String<2>
fn u8_to_hex_string(input: u8) -> String<2> {
    let mut s_hex: String<2> = String::new();

    const HEX_CHARS: &[u8] = b"0123456789ABCDEF";
    let high_nibble = (input >> 4) & 0xF;
    let low_nibble = input & 0xF;
    let hh = [
        HEX_CHARS[high_nibble as usize],
        HEX_CHARS[low_nibble as usize],
    ];

    let _ = s_hex.push_str(str::from_utf8(&[hh[0]]).unwrap());
    let _ = s_hex.push_str(str::from_utf8(&[hh[1]]).unwrap());

    s_hex
}

// The leftpad_u32_to_string function takes a u32 input representing an number
// and a padding character, and formats the number with leading padding characters
// if necessary. The resulting formatted number is returned as a fixed-size String
// with a length of N.
pub fn leftpad_u32_to_string<const N: usize>(number: u32, fill_char: char) -> String<N> {
    let mut formatted_number = String::<N>::new();
    let s_number: String<N> = String::try_from(number).unwrap();

    for _ in 0..(N - s_number.len()) {
        formatted_number.push(fill_char).unwrap();
    }
    formatted_number.push_str(&s_number).unwrap();

    formatted_number
}
