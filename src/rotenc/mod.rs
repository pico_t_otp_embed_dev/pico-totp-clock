use embassy_rp::gpio::Pull;
use embassy_rp::peripherals::PIO0;
use embassy_rp::pio;

use pio::{Common, Config as PioConfig, FifoJoin, Instance, PioPin, ShiftDirection, StateMachine};

use core::sync::atomic::{AtomicBool, AtomicU32, Ordering};
use fixed::traits::ToFixed;

use crate::nfc::LOCK;

pub static COUNT: AtomicU32 = AtomicU32::new(0); // Count number of encoder move from 0 to max_count
pub static SWITCH_PRESSED: AtomicBool = AtomicBool::new(false);

pub enum Direction {
    Clockwise,
    CounterClockwise,
}

pub struct PioEncoder<'d, T: Instance, const SM: usize> {
    sm: StateMachine<'d, T, SM>,
}

impl<'d, T: Instance, const SM: usize> PioEncoder<'d, T, SM> {
    pub fn new(
        pio: &mut Common<'d, T>,
        mut sm: StateMachine<'d, T, SM>,
        pin_a: impl PioPin, // CLK
        pin_b: impl PioPin, // Data
    ) -> Self {
        let mut pin_a = pio.make_pio_pin(pin_a);
        let mut pin_b = pio.make_pio_pin(pin_b);

        pin_a.set_pull(Pull::Up);
        pin_b.set_pull(Pull::Up);

        sm.set_pin_dirs(pio::Direction::In, &[&pin_a, &pin_b]);

        let prg = pio_proc::pio_asm!(
            "wait 1 pin 1",
            "mov y y [31]", // wait 32cycle
            "mov y y [31]", // wait 32cycle
            "mov y y [31]", // wait 32cycle
            "mov y y [31]", // wait 32cycle
            "mov y y [31]", // wait 32cycle
            "mov y y [31]", // wait 32cycle
            "mov y y [31]", // wait 32cycle
            "wait 0 pin 1",
            "in pins, 2",
            "push",
        );

        let mut cfg = PioConfig::default();
        cfg.set_in_pins(&[&pin_a, &pin_b]);
        cfg.fifo_join = FifoJoin::RxOnly;
        cfg.shift_in.direction = ShiftDirection::Left;
        cfg.clock_divider = 6250.to_fixed();
        cfg.use_program(&pio.load_program(&prg.program), &[]);
        sm.set_config(&cfg);
        sm.set_enable(true);
        Self { sm }
    }

    async fn read(&mut self) -> Direction {
        loop {
            match self.sm.rx().wait_pull().await {
                0 => return Direction::Clockwise,
                1 => return Direction::CounterClockwise,
                _ => {}
            }
        }
    }
}

pub struct PioSwitch<'d, T: Instance, const SM: usize> {
    sm: StateMachine<'d, T, SM>,
}

impl<'d, T: Instance, const SM: usize> PioSwitch<'d, T, SM> {
    pub fn new(
        pio: &mut Common<'d, T>,
        mut sm: StateMachine<'d, T, SM>,
        pin_a: impl PioPin, // Switch pin
    ) -> Self {
        let mut pin_a = pio.make_pio_pin(pin_a);
        pin_a.set_pull(Pull::Up);
        sm.set_pin_dirs(pio::Direction::In, &[&pin_a]);

        let prg = pio_proc::pio_asm!(
            "wait 0 pin 0",
            "mov y y [31]", // wait 32cycle
            "mov y y [31]", // wait 32cycle
            "wait 1 pin 0",
            "in pins, 1",
            "push",
        );

        let mut cfg = PioConfig::default();
        cfg.set_in_pins(&[&pin_a]);
        cfg.shift_in.direction = ShiftDirection::Left;
        cfg.clock_divider = 12500.to_fixed();
        cfg.use_program(&pio.load_program(&prg.program), &[]);
        sm.set_config(&cfg);
        sm.set_enable(true);

        Self { sm }
    }

    async fn read(&mut self) -> u32 {
        self.sm.rx().wait_pull().await
    }
}

#[embassy_executor::task]
pub async fn encoder_count_task(encoder: &'static mut PioEncoder<'_, PIO0, 1>, max_count: u32) {
    let mut count;

    loop {
        count += match encoder.read().await {
            Direction::Clockwise => {
                count = COUNT.load(Ordering::Relaxed) as i32;
                if count < max_count as i32 && !LOCK.load(Ordering::Relaxed) {
                    1
                } else {
                    0
                }
            }
            Direction::CounterClockwise => {
                count = COUNT.load(Ordering::Relaxed) as i32;
                if count > 0 && !LOCK.load(Ordering::Relaxed) {
                    -1
                } else {
                    0
                }
            }
        };
        COUNT.store(count as u32, Ordering::Relaxed);
        //info!("Count = {}", count);
    }
}

#[embassy_executor::task]
pub async fn encoder_switch_task(switch: &'static mut PioSwitch<'_, PIO0, 2>) {
    loop {
        switch.read().await;
        //info!("Switch pressed !!!");
        if COUNT.load(Ordering::Relaxed) > 0 {
            SWITCH_PRESSED.store(true, Ordering::Release);
        } else {
            SWITCH_PRESSED.store(false, Ordering::Release);
        }
    }
}
