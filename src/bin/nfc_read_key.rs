#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(impl_trait_in_assoc_type)]

use defmt::*;
use defmt_rtt as _;
use panic_probe as _;

use core::cell::RefCell;

use embassy_embedded_hal::shared_bus::blocking::spi::SpiDevice;
use embassy_executor::Spawner;
use embassy_sync::blocking_mutex::{raw::NoopRawMutex, NoopMutex};
use embassy_time::{Duration, Timer};

use embassy_rp::peripherals::SPI0;
use embassy_rp::spi::{Async, Spi};
use embassy_rp::{gpio, spi};

use gpio::{Level, Output};
use static_cell::make_static;

// NFC
use mfrc522::comm::blocking::spi::{DummyDelay, SpiInterface};
use mfrc522::error::Error;
use mfrc522::{Initialized, Mfrc522, Uid};

type MC522 = Mfrc522<
    SpiInterface<
        SpiDevice<'static, NoopRawMutex, Spi<'static, SPI0, Async>, Output<'static>>,
        DummyDelay,
    >,
    Initialized,
>;

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
    let p = embassy_rp::init(Default::default());

    let miso = p.PIN_4;
    let mosi = p.PIN_7;
    let clk = p.PIN_6;
    let nfc_cs = p.PIN_5;
    let mut reset = Output::new(p.PIN_2, Level::High);

    // Mini module pull reset high first
    reset.set_high();

    // create SPI
    let mut nfc_config = spi::Config::default();
    nfc_config.frequency = 1_000_000;

    let spi = Spi::new(p.SPI0, clk, mosi, miso, p.DMA_CH0, p.DMA_CH1, nfc_config);

    let spi_bus = make_static!(NoopMutex::new(RefCell::new(spi)));

    let nfc_spi = SpiDevice::new(spi_bus, Output::new(nfc_cs, Level::High));

    let itf = SpiInterface::new(nfc_spi);

    let mfrc522 = make_static!(Mfrc522::new(itf).init().unwrap());

    let version = mfrc522.version().unwrap();
    info!("VERSION: {}", version);

    loop {
        if let Ok(atqa) = mfrc522.wupa() {
            defmt::info!("new card detected");
            match mfrc522.select(&atqa) {
                // Ok(ref uid @ Uid::Single(ref inner)) => {
                //     defmt::info!("card uid {:#04X}", inner.as_bytes());
                //     // handle_card(&mut mfrc522, &uid, write);
                // }
                Ok(ref _uid @ Uid::Double(ref inner)) => {
                    defmt::info!("card double uid {:#04X}", inner.as_bytes());
                    handle_card(mfrc522).await;
                }
                Ok(_) => defmt::info!("got other uid size"),
                Err(e) => {
                    defmt::error!("Select error");
                    print_err(&e);
                }
            }
        }
        Timer::after(Duration::from_millis(3000)).await;

        info!("Done !!!");
    }
}

async fn handle_card(mfrc522: &mut MC522) {
    let mut rx_buffer = [0x00u8; 32];

    match mfrc522.mf_read(0x10) {
        Ok(data) => {
            // defmt::info!("read {:#04X}", data);
            rx_buffer[..16].copy_from_slice(&data);
        }
        Err(e) => {
            defmt::error!("error during read");
            print_err(&e);
        }
    }

    match mfrc522.mf_read(0x14) {
        Ok(data) => {
            rx_buffer[16..(16 + 16)].copy_from_slice(&data);
        }
        Err(e) => {
            defmt::error!("error during read");
            print_err(&e);
        }
    }

    info!("rx_buffer = {:#04X}", rx_buffer);

    if mfrc522.hlta().is_err() {
        defmt::error!("Could not halt");
    }
}

fn print_err<E>(err: &Error<E>) {
    match err {
        Error::Bcc => defmt::error!("error BCC"),
        Error::BufferOverflow => defmt::error!("error BufferOverflow"),
        Error::Collision => defmt::error!("error Collision"),
        Error::Crc => defmt::error!("error Crc"),
        Error::IncompleteFrame => defmt::error!("error Incomplete"),
        Error::NoRoom => defmt::error!("error NoRoom"),
        Error::Overheating => defmt::error!("error Overheating"),
        Error::Parity => defmt::error!("error Parity"),
        Error::Protocol => defmt::error!("error Protocol"),
        Error::Timeout => defmt::error!("error Timeout"),
        Error::Wr => defmt::error!("error Wr"),
        Error::Nak => defmt::error!("error Nak"),
        _ => defmt::error!("error SPI"),
    };
}
