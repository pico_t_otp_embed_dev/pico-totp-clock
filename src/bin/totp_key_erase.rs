#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(impl_trait_in_assoc_type)]

use defmt::*;
use {defmt_rtt as _, panic_probe as _};

use embassy_executor::Spawner;
use embassy_time::{Duration, Timer};

use embassy_rp::i2c::{self, Config};

use eeprom24x::{Eeprom24x, SlaveAddr};
use panic_probe::hard_fault;

// AT24C08 8Kbit (1024byte)
const E_PAGE_SIZE: u32 = 16;

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
    let p = embassy_rp::init(Default::default());

    let sda = p.PIN_14;
    let scl = p.PIN_15;

    let i2c = i2c::I2c::new_blocking(p.I2C1, scl, sda, Config::default());

    let mut eeprom = Eeprom24x::new_24x08(i2c, SlaveAddr::default());

    let mut memory_address = 0;
    let erase_data: [u8; 1024] = [255; 1024];

    for val in erase_data.chunks(E_PAGE_SIZE as usize) {
        let _ = eeprom.write_page(memory_address, val); // WARN Result ignored
        memory_address += E_PAGE_SIZE;

        if memory_address % 32 == 0 {
            info!("Erasing eeprom...");
        }

        let delay = Duration::from_millis(5);
        Timer::after(delay).await;

        // Stop at adress 944 to keep SSID, WIFI PASS and NTP IP and NFC TAG information
        if memory_address == 944 {
            break;
        }
    }

    let _ = eeprom.write_byte(1023, 0);

    let delay = Duration::from_millis(5);
    Timer::after(delay).await;

    info!("done");

    let _dev = eeprom.destroy(); // Get the I2C device back

    hard_fault();
}
