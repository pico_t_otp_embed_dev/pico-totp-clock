#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(impl_trait_in_assoc_type)]

use defmt::*;
use {defmt_rtt as _, panic_probe as _};

use embassy_executor::Spawner;
use embassy_time::{Duration, Timer};

use embassy_rp::flash::{Async, Flash};
use embassy_rp::{
    gpio::{Input, Pull},
    Peripheral,
};

use pico_totp_clock::unsafe_p_clone;
use pico_totp_clock::usb_net::usb_net;
use pico_totp_clock::{init_serial_number, totp_clock::totp_clock};

pub const FLASH_SIZE: usize = 2 * 1024 * 1024;

#[derive(PartialEq)]
enum AppState {
    TotpClock,
    UsbNet,
}

#[embassy_executor::main]
async fn main(spawner: Spawner) {
    let mut p = embassy_rp::init(Default::default());

    // Read and init serial number
    let mut sn = [0; 8];
    let mut flash =
        Flash::<_, Async, FLASH_SIZE>::new(unsafe_p_clone!(p.FLASH), unsafe_p_clone!(p.DMA_CH0));
    flash.blocking_unique_id(&mut sn).unwrap();
    init_serial_number(sn).await;

    // Test initial rotary encoder button status
    let re_button = unsafe { Input::new(p.PIN_22.clone_unchecked(), Pull::Up) };
    Timer::after(Duration::from_millis(1000)).await; // Wait 1 sec

    // get app state
    let app_state: AppState = match re_button.is_high() {
        true => AppState::TotpClock,
        false => AppState::UsbNet,
    };

    // Run application mode, TotpClock or UsbNet
    match app_state {
        AppState::TotpClock => {
            info!("TotpClock state !!!");
            totp_clock(&spawner, &mut p).await;
        }
        AppState::UsbNet => {
            info!("UsbNet state !!!");
            usb_net(&spawner, &mut p).await;
        }
    }
}
