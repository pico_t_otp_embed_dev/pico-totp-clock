#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]
#![feature(impl_trait_in_assoc_type)]

use defmt::*;
use defmt_rtt as _;
use panic_probe::{self as _, hard_fault};

use core::cell::RefCell;

use embassy_embedded_hal::shared_bus::blocking::spi::SpiDevice;
use embassy_executor::Spawner;
use embassy_sync::blocking_mutex::{raw::NoopRawMutex, NoopMutex};
use embassy_time::{Duration, Timer};

use embassy_rp::peripherals::SPI0;
use embassy_rp::spi::{Async, Spi};
use embassy_rp::{gpio, spi};

use gpio::{Level, Output};
use static_cell::make_static;

// NFC
use mfrc522::comm::blocking::spi::{DummyDelay, SpiInterface};
use mfrc522::error::Error;
use mfrc522::{Initialized, Mfrc522, Uid};

type MC522 = Mfrc522<
    SpiInterface<
        SpiDevice<'static, NoopRawMutex, Spi<'static, SPI0, Async>, Output<'static>>,
        DummyDelay,
    >,
    Initialized,
>;

#[embassy_executor::main]
async fn main(_spawner: Spawner) {
    let p = embassy_rp::init(Default::default());

    let miso = p.PIN_4;
    let mosi = p.PIN_7;
    let clk = p.PIN_6;
    let nfc_cs = p.PIN_5;
    let mut reset = Output::new(p.PIN_2, Level::High);

    // Mini module pull reset high first
    reset.set_high();

    // create SPI
    let mut nfc_config = spi::Config::default();
    nfc_config.frequency = 1_000_000;

    let spi = Spi::new(p.SPI0, clk, mosi, miso, p.DMA_CH0, p.DMA_CH1, nfc_config);

    let spi_bus = make_static!(NoopMutex::new(RefCell::new(spi)));

    let nfc_spi = SpiDevice::new(spi_bus, Output::new(nfc_cs, Level::High));

    let itf = SpiInterface::new(nfc_spi);

    let mfrc522 = make_static!(Mfrc522::new(itf).init().unwrap());

    let version = mfrc522.version().unwrap();
    info!("VERSION: {}", version);

    loop {
        if let Ok(atqa) = mfrc522.wupa() {
            defmt::info!("new card detected");
            match mfrc522.select(&atqa) {
                Ok(ref _uid @ Uid::Double(ref inner)) => {
                    defmt::info!("card double uid {:#04X}", inner.as_bytes());
                    handle_card(mfrc522).await;
                    info!("Done !!!");
                    hard_fault();
                }
                Ok(_) => defmt::info!("got other uid size"),
                Err(e) => {
                    defmt::error!("Select error");
                    print_err(&e);
                }
            }
        }
        Timer::after(Duration::from_millis(3000)).await;
    }
}

async fn handle_card(mfrc522: &mut MC522) {
    // Add ChaCha20 Secret key on the nfc tag
    //
    // To create secret_key, on linux do :
    //
    // echo 'let secret_key = [' && for x in `xxd -u -l 32 -p -c 1 /dev/urandom`; do echo -n 0x$x,; done && echo && echo '];'
    //
    let secret_key = [
        0x31, 0x36, 0x30, 0x30, 0x43, 0x37, 0x34, 0x31, 0x32, 0x33, 0x41, 0x43, 0x31, 0x34, 0x33,
        0x36, 0x30, 0x46, 0x46, 0x44, 0x36, 0x33, 0x30, 0x31, 0x38, 0x41, 0x33, 0x46, 0x46, 0x33,
        0x41, 0x38,
    ];

    let mut fixed_size_chunk: [u8; 16] = [0; 16];
    fixed_size_chunk[0] = secret_key[0];
    fixed_size_chunk[1] = secret_key[1];
    fixed_size_chunk[2] = secret_key[2];
    fixed_size_chunk[3] = secret_key[3];

    match mfrc522.mf_write(0x10_u8, fixed_size_chunk) {
        Ok(_) => {
            defmt::info!("write success");
        }
        Err(e) => {
            defmt::error!("error during write");
            print_err(&e);
        }
    }

    let mut fixed_size_chunk: [u8; 16] = [0; 16];
    fixed_size_chunk[0] = secret_key[4];
    fixed_size_chunk[1] = secret_key[5];
    fixed_size_chunk[2] = secret_key[6];
    fixed_size_chunk[3] = secret_key[7];

    match mfrc522.mf_write(0x11_u8, fixed_size_chunk) {
        Ok(_) => {
            defmt::info!("write success");
        }
        Err(e) => {
            defmt::error!("error during write");
            print_err(&e);
        }
    }

    let mut fixed_size_chunk: [u8; 16] = [0; 16];
    fixed_size_chunk[0] = secret_key[8];
    fixed_size_chunk[1] = secret_key[9];
    fixed_size_chunk[2] = secret_key[10];
    fixed_size_chunk[3] = secret_key[11];

    match mfrc522.mf_write(0x12_u8, fixed_size_chunk) {
        Ok(_) => {
            defmt::info!("write success");
        }
        Err(e) => {
            defmt::error!("error during write");
            print_err(&e);
        }
    }

    let mut fixed_size_chunk: [u8; 16] = [0; 16];
    fixed_size_chunk[0] = secret_key[12];
    fixed_size_chunk[1] = secret_key[13];
    fixed_size_chunk[2] = secret_key[14];
    fixed_size_chunk[3] = secret_key[15];

    match mfrc522.mf_write(0x13_u8, fixed_size_chunk) {
        Ok(_) => {
            defmt::info!("write success");
        }
        Err(e) => {
            defmt::error!("error during write");
            print_err(&e);
        }
    }

    let mut fixed_size_chunk: [u8; 16] = [0; 16];
    fixed_size_chunk[0] = secret_key[16];
    fixed_size_chunk[1] = secret_key[17];
    fixed_size_chunk[2] = secret_key[18];
    fixed_size_chunk[3] = secret_key[19];

    match mfrc522.mf_write(0x14_u8, fixed_size_chunk) {
        Ok(_) => {
            defmt::info!("write success");
        }
        Err(e) => {
            defmt::error!("error during write");
            print_err(&e);
        }
    }

    let mut fixed_size_chunk: [u8; 16] = [0; 16];
    fixed_size_chunk[0] = secret_key[20];
    fixed_size_chunk[1] = secret_key[21];
    fixed_size_chunk[2] = secret_key[22];
    fixed_size_chunk[3] = secret_key[23];

    match mfrc522.mf_write(0x15_u8, fixed_size_chunk) {
        Ok(_) => {
            defmt::info!("write success");
        }
        Err(e) => {
            defmt::error!("error during write");
            print_err(&e);
        }
    }

    let mut fixed_size_chunk: [u8; 16] = [0; 16];
    fixed_size_chunk[0] = secret_key[24];
    fixed_size_chunk[1] = secret_key[25];
    fixed_size_chunk[2] = secret_key[26];
    fixed_size_chunk[3] = secret_key[27];

    match mfrc522.mf_write(0x16_u8, fixed_size_chunk) {
        Ok(_) => {
            defmt::info!("write success");
        }
        Err(e) => {
            defmt::error!("error during write");
            print_err(&e);
        }
    }

    let mut fixed_size_chunk: [u8; 16] = [0; 16];
    fixed_size_chunk[0] = secret_key[28];
    fixed_size_chunk[1] = secret_key[29];
    fixed_size_chunk[2] = secret_key[30];
    fixed_size_chunk[3] = secret_key[31];

    match mfrc522.mf_write(0x17_u8, fixed_size_chunk) {
        Ok(_) => {
            defmt::info!("write success");
        }
        Err(e) => {
            defmt::error!("error during write");
            print_err(&e);
        }
    }

    if mfrc522.hlta().is_err() {
        defmt::error!("Could not halt");
    }
}

fn print_err<E>(err: &Error<E>) {
    match err {
        Error::Bcc => defmt::error!("error BCC"),
        Error::BufferOverflow => defmt::error!("error BufferOverflow"),
        Error::Collision => defmt::error!("error Collision"),
        Error::Crc => defmt::error!("error Crc"),
        Error::IncompleteFrame => defmt::error!("error Incomplete"),
        Error::NoRoom => defmt::error!("error NoRoom"),
        Error::Overheating => defmt::error!("error Overheating"),
        Error::Parity => defmt::error!("error Parity"),
        Error::Protocol => defmt::error!("error Protocol"),
        Error::Timeout => defmt::error!("error Timeout"),
        Error::Wr => defmt::error!("error Wr"),
        Error::Nak => defmt::error!("error Nak"),
        _ => defmt::error!("error SPI"),
    };
}
