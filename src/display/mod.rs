use embassy_rp::i2c::{Async, I2c};
use embassy_rp::peripherals::I2C0;

use embedded_graphics::{
    mono_font::{
        iso_8859_15::FONT_7X13_BOLD, iso_8859_15::FONT_8X13_BOLD, iso_8859_15::FONT_9X15_BOLD,
        MonoTextStyleBuilder,
    },
    pixelcolor::BinaryColor,
    prelude::*,
    primitives::{Line, PrimitiveStyle, PrimitiveStyleBuilder, Rectangle},
    text::{Alignment, Text},
};

use eg_seven_segment::SevenSegmentStyleBuilder;
use heapless::String;
use ssd1306::{mode::BufferedGraphicsMode, prelude::*, Ssd1306};

use crate::leftpad_u32_to_string;

pub type Display128x64 = Ssd1306<
    I2CInterface<I2c<'static, I2C0, Async>>,
    DisplaySize128x64,
    BufferedGraphicsMode<DisplaySize128x64>,
>;

// To print waiting dot ...
pub fn print_dot(display: &mut Display128x64, nb_dot: u32) {
    display.clear_buffer();

    let mut s_dot = String::<14>::new();
    for _ in 0..nb_dot {
        let _ = s_dot.push('·');
    }

    let txt_style = MonoTextStyleBuilder::new()
        .font(&FONT_9X15_BOLD)
        .text_color(BinaryColor::On)
        .build();

    Text::with_alignment(&s_dot, Point::new(64, 32), txt_style, Alignment::Center)
        .draw(display)
        .unwrap();

    display.flush().unwrap();
}

// To display title bar
pub fn print_title(display: &mut Display128x64, title: &str) {
    let txt_style = MonoTextStyleBuilder::new()
        .font(&FONT_9X15_BOLD)
        .text_color(BinaryColor::On)
        .build();

    Text::with_alignment(title, Point::new(64, 12), txt_style, Alignment::Center)
        .draw(display)
        .unwrap();
}

// Print app info: version, mac address and network ip
pub fn print_info(display: &mut Display128x64, ver: &String<5>, mac: &String<17>, ip: &String<15>) {
    let mut version: String<6> = String::new();
    version.push('v').unwrap();
    version.push_str(ver).unwrap();

    display.clear_buffer();

    print_title(display, "Totp Clock");

    let txt_style = MonoTextStyleBuilder::new()
        .font(&FONT_8X13_BOLD)
        .text_color(BinaryColor::On)
        .build();

    let macadd_style = MonoTextStyleBuilder::new()
        .font(&FONT_7X13_BOLD)
        .text_color(BinaryColor::On)
        .build();

    Text::with_alignment(&version, Point::new(63, 28), txt_style, Alignment::Center)
        .draw(display)
        .unwrap();

    Text::with_alignment(mac, Point::new(63, 44), macadd_style, Alignment::Center)
        .draw(display)
        .unwrap();

    Text::with_alignment(ip, Point::new(63, 63), macadd_style, Alignment::Center)
        .draw(display)
        .unwrap();

    display.flush().unwrap();
}

// To display seconds progress bar
pub fn print_seconds_progress_bar(display: &mut Display128x64, second: u32) {
    let sstyle: PrimitiveStyle<BinaryColor> = PrimitiveStyleBuilder::new()
        .stroke_color(BinaryColor::On)
        .stroke_width(2)
        .build();

    let style_line_off: PrimitiveStyle<BinaryColor> = PrimitiveStyleBuilder::new()
        .stroke_color(BinaryColor::Off)
        .stroke_width(2)
        .build();

    let py = 62;

    if second > 0 {
        Line::new(
            Point::new(5, py),
            Point::new(((second * 2 + 5) - 1) as i32, py),
        )
        .into_styled(sstyle)
        .draw(display)
        .unwrap();
    } else {
        // Clear line
        Line::new(Point::new(5, py), Point::new(122, py))
            .into_styled(style_line_off)
            .draw(display)
            .unwrap();

        Line::new(Point::new(63, 60), Point::new(63, 64))
            .into_styled(style_line_off)
            .draw(display)
            .unwrap();
    }
}

// Display clock
pub fn print_clock(
    display: &mut Display128x64,
    hour: u32,
    minute: u32,
    second: u32,
    year: i32,
    month: u32,
    day: u32,
) {
    let hh = leftpad_u32_to_string::<2>(hour, ' '); // hours string
    let mm = leftpad_u32_to_string::<2>(minute, '0'); // minutes string

    // Align midle position
    let xp = match hour {
        20..=23 => 63,
        10..=19 => 55,
        1 => 45,
        _ => 51,
    };

    display.clear_buffer();

    // Clear clock area
    let rstyle: PrimitiveStyle<BinaryColor> = PrimitiveStyleBuilder::new()
        .stroke_color(BinaryColor::Off)
        .fill_color(BinaryColor::Off)
        .stroke_width(1)
        .build();
    Rectangle::new(Point::new(0, 16), Size::new(128, 44))
        .into_styled(rstyle)
        .draw(display)
        .unwrap();

    // Define clock text style
    let cstyle = SevenSegmentStyleBuilder::new()
        .digit_size(Size::new(17, 34)) // digits are 10x20 pixels
        .digit_spacing(4) // 5px spacing between digits
        .segment_width(2) // 5px wide segments
        .segment_color(BinaryColor::On)
        .build();

    let mut s_date = String::<14>::new();
    let ye: String<4> = String::try_from(year).unwrap();
    let _ = s_date.push_str(&ye);
    let _ = s_date.push('·');
    let mo = leftpad_u32_to_string::<2>(month, '0');
    let _ = s_date.push_str(&mo);
    let _ = s_date.push('·');
    let da = leftpad_u32_to_string::<2>(day, '0');
    let _ = s_date.push_str(&da);
    print_title(display, &s_date);

    // Display clock
    Text::with_alignment(&hh, Point::new(xp - 22, 53), cstyle, Alignment::Center)
        .draw(display)
        .unwrap();
    Text::with_alignment(":", Point::new(xp, 53), cstyle, Alignment::Center)
        .draw(display)
        .unwrap();
    Text::with_alignment(&mm, Point::new(xp + 22, 53), cstyle, Alignment::Center)
        .draw(display)
        .unwrap();

    let rstyle: PrimitiveStyle<BinaryColor> = PrimitiveStyleBuilder::new()
        .stroke_color(BinaryColor::On)
        .stroke_width(2)
        .build();
    Line::new(Point::new(3, 60), Point::new(3, 64))
        .into_styled(rstyle)
        .draw(display)
        .unwrap();
    Line::new(Point::new(123, 60), Point::new(123, 64))
        .into_styled(rstyle)
        .draw(display)
        .unwrap();

    print_seconds_progress_bar(display, second);

    // // Debug square
    // let sqstyle: PrimitiveStyle<BinaryColor> = PrimitiveStyleBuilder::new()
    //   .stroke_color(BinaryColor::On)
    //   .stroke_width(1)
    //   .build();
    // Line::new(Point::new(0, 0), Point::new(127, 0)).into_styled(sqstyle).draw(display).unwrap();
    // Line::new(Point::new(127, 0), Point::new(127, 63)).into_styled(sqstyle).draw(display).unwrap();
    // Line::new(Point::new(0, 0), Point::new(0, 63)).into_styled(sqstyle).draw(display).unwrap();
    // Line::new(Point::new(0, 63), Point::new(127, 63)).into_styled(sqstyle).draw(display).unwrap();

    display.flush().unwrap();
}

pub fn print_totp(display: &mut Display128x64, title: &String<14>, totp: &String<6>, second: u32) {
    display.clear_buffer();

    // Title style
    let title_text_style = MonoTextStyleBuilder::new()
        .font(&FONT_9X15_BOLD)
        .text_color(BinaryColor::On)
        .build();
    // Define a new style.
    let ss_style = SevenSegmentStyleBuilder::new()
        .digit_size(Size::new(17, 34)) // digits are 10x20 pixels
        .digit_spacing(4) // 5px spacing between digits
        .segment_width(2) // 5px wide segments
        .segment_color(BinaryColor::On)
        .build();

    Text::with_alignment(
        title,
        Point::new(63, 12),
        title_text_style,
        Alignment::Center,
    )
    .draw(display)
    .unwrap();

    // Use the style to draw text to a embedded-graphics `DrawTarget`.
    Text::new(totp, Point::new(3, 53), ss_style)
        .draw(display)
        .unwrap();

    let rstyle: PrimitiveStyle<BinaryColor> = PrimitiveStyleBuilder::new()
        .stroke_color(BinaryColor::On)
        .stroke_width(2)
        .build();
    Line::new(Point::new(63, 60), Point::new(63, 64))
        .into_styled(rstyle)
        .draw(display)
        .unwrap();
    Line::new(Point::new(3, 60), Point::new(3, 64))
        .into_styled(rstyle)
        .draw(display)
        .unwrap();
    Line::new(Point::new(123, 60), Point::new(123, 64))
        .into_styled(rstyle)
        .draw(display)
        .unwrap();

    print_seconds_progress_bar(display, second);

    display.flush().unwrap();
}
