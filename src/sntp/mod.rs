use arbitrary_int::{u2, u3};
use bitbybit::{bitenum, bitfield};
use byteorder::{BigEndian, ByteOrder};

#[bitenum(u2, exhaustive: true)]
pub enum LeapIndicator {
    NoWarning = 0b00,
    LastMinute61Sec = 0b01,
    LastMinute59Sec = 0b10,
    AlarmCondition = 0b11,
}

#[bitenum(u3, exhaustive: true)]
pub enum ProtocolMode {
    Reserved = 0b000,
    SymmetricActive = 0b001,
    SymmetricPassive = 0b010,
    Client = 0b011,
    Server = 0b100,
    Broadcast = 0b101,
    NtpControlMessage = 0b110,
    Private = 0b111,
}

#[repr(u8)]
pub enum Stratum {
    KissOfDeath,
    Primary,
    Secondary(u8),
    Reserved(u8),
}

impl From<u8> for Stratum {
    fn from(s: u8) -> Self {
        match s {
            0 => Stratum::KissOfDeath,
            1 => Stratum::Primary,
            2..=15 => Stratum::Secondary(s),
            _ => Stratum::Reserved(s),
        }
    }
}

impl Into<u8> for Stratum {
    fn into(self) -> u8 {
        match self {
            Stratum::KissOfDeath => 0,
            Stratum::Primary => 1,
            Stratum::Secondary(s) => s,
            Stratum::Reserved(s) => s,
        }
    }
}

#[bitfield(u64, default: 0)]
pub struct Timestamp {
    #[bits(32..=63, rw)]
    pub sec: u32,
    #[bits(0..=31, rw)]
    pub frac: u32,
}

// LI, VN, Mode, Stratum, Pool, Precision
#[bitfield(u32, default: 0)]
pub struct LVMSPP {
    #[bits(30..=31, rw)]
    pub leap_indicator: u2,
    #[bits(27..=29, rw)]
    pub version: u3,
    #[bits(24..=26, rw)]
    pub protocol_mode: u3,
    #[bits(16..=23, rw)]
    pub stratum: u8,
    #[bits(8..=15, rw)]
    pub pool_interval: u8,
    #[bits(0..=7, rw)]
    pub precision: i8,
}

#[derive(Clone, Copy)]
#[repr(C)]
pub struct SNTP {
    pub lvmspp: LVMSPP,
    pub root_delay: i32,
    pub root_dispersion: u32,
    pub ref_identifier: [u8; 4], // 32bit
    pub ref_timestamp: Timestamp,
    pub orig_timestamp: Timestamp,
    pub recv_timestamp: Timestamp,
    pub xmit_timestamp: Timestamp,
}

impl SNTP {
    pub fn to_bytes(&self) -> [u8; core::mem::size_of::<SNTP>()] {
        let mut bytes: [u8; core::mem::size_of::<SNTP>()] = [0; core::mem::size_of::<SNTP>()];

        BigEndian::write_u32(&mut bytes[0..4], self.lvmspp.raw_value);
        BigEndian::write_i32(&mut bytes[4..8], self.root_delay);
        BigEndian::write_u32(&mut bytes[8..12], self.root_dispersion);
        bytes[12..16].copy_from_slice(&self.ref_identifier);
        BigEndian::write_u64(&mut bytes[16..24], self.ref_timestamp.raw_value);
        BigEndian::write_u64(&mut bytes[24..32], self.orig_timestamp.raw_value);
        BigEndian::write_u64(&mut bytes[32..40], self.recv_timestamp.raw_value);
        BigEndian::write_u64(&mut bytes[40..48], self.xmit_timestamp.raw_value);

        bytes
    }

    pub fn from_bytes(bytes: &[u8]) -> Option<Self> {
        if bytes.len() != core::mem::size_of::<SNTP>() {
            return None;
        }

        let mut lvmspp_bytes = [0u8; core::mem::size_of::<u32>()];
        let mut root_delay_bytes = [0u8; core::mem::size_of::<i32>()];
        let mut root_dispersion_bytes = [0u8; core::mem::size_of::<u32>()];
        let mut ref_identifier_bytes = [0u8; 4];
        let mut ref_timestamp_bytes = [0u8; core::mem::size_of::<u64>()];
        let mut orig_timestamp_bytes = [0u8; core::mem::size_of::<u64>()];
        let mut recv_timestamp_bytes = [0u8; core::mem::size_of::<u64>()];
        let mut xmit_timestamp_bytes = [0u8; core::mem::size_of::<u64>()];

        lvmspp_bytes.copy_from_slice(&bytes[0..4]);
        root_delay_bytes.copy_from_slice(&bytes[4..8]);
        root_dispersion_bytes.copy_from_slice(&bytes[8..12]);
        ref_identifier_bytes.copy_from_slice(&bytes[12..16]);
        ref_timestamp_bytes.copy_from_slice(&bytes[16..24]);
        orig_timestamp_bytes.copy_from_slice(&bytes[24..32]);
        recv_timestamp_bytes.copy_from_slice(&bytes[32..40]);
        xmit_timestamp_bytes.copy_from_slice(&bytes[40..48]);

        let lvmspp = u32::from_be_bytes(lvmspp_bytes);
        let root_delay = i32::from_be_bytes(root_delay_bytes);
        let root_dispersion = u32::from_be_bytes(root_dispersion_bytes);
        let ref_timestamp = u64::from_be_bytes(ref_timestamp_bytes);
        let orig_timestamp = u64::from_be_bytes(orig_timestamp_bytes);
        let recv_timestamp = u64::from_be_bytes(recv_timestamp_bytes);
        let xmit_timestamp = u64::from_be_bytes(xmit_timestamp_bytes);

        Some(SNTP {
            lvmspp: LVMSPP { raw_value: lvmspp },
            root_delay,
            root_dispersion,
            ref_identifier: ref_identifier_bytes,
            ref_timestamp: Timestamp {
                raw_value: ref_timestamp,
            },
            orig_timestamp: Timestamp {
                raw_value: orig_timestamp,
            },
            recv_timestamp: Timestamp {
                raw_value: recv_timestamp,
            },
            xmit_timestamp: Timestamp {
                raw_value: xmit_timestamp,
            },
        })
    }
}
