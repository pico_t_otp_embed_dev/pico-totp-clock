use defmt::*;

use core::ops::DerefMut;
use core::str;
use core::sync::atomic::Ordering;

use embassy_net::Stack;
use embassy_time::Duration;
use embassy_usb::class::cdc_ncm::embassy_net::Device as CdcNcmNetDevice;

use heapless::{String, Vec};
use picoserve::routing::{get, parse_path_segment, post};
use serde::{Deserialize, Serialize};
use static_cell::make_static;

use ee24x08::{self as EE, Ee24x08};

use crate::enc::encrypt_key;
use crate::nfc::LOCK;
use crate::totp_clock::read_totp_from_eeprom;
use crate::{
    init_nfc_tag_id, mtx_deref_mut, u8_to_hex_string, usb_net::MTU, EeMtx, EE_CFG_ADD, EE_NET_CFG,
};

struct EmbassyTimer;

impl picoserve::Timer for EmbassyTimer {
    type Duration = embassy_time::Duration;
    type TimeoutError = embassy_time::TimeoutError;

    async fn run_with_timeout<F: core::future::Future>(
        &mut self,
        duration: Self::Duration,
        future: F,
    ) -> Result<F::Output, Self::TimeoutError> {
        embassy_time::with_timeout(duration, future).await
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TotpSecretKeyForm {
    pub title: String<14>,
    pub key: String<32>,
}

#[derive(Serialize, Deserialize)]
struct NetConfigValue {
    ssid: String<{ EE_NET_CFG::LEN }>,
    pass: String<{ EE_NET_CFG::LEN }>,
    ntp: String<{ EE_NET_CFG::LEN }>,
}

#[derive(Serialize, Deserialize)]
struct NfcTag {
    id: String<14>,
}

// Function to convert a pair of hexadecimal characters to u8
fn hex_to_u8(hex_chars: &[u8; 2]) -> Option<u8> {
    let mut result = 0;
    for &c in hex_chars {
        let digit = match c {
            b'0'..=b'9' => c - b'0',
            b'A'..=b'F' => c - b'A' + 10,
            b'a'..=b'f' => c - b'a' + 10,
            _ => return None, // Invalid hex character
        };
        result = result * 16 + digit;
    }
    Some(result)
}

// Write bytes to eeprom
async fn write_bytes_to_eeprom(ee: &mut Ee24x08, ee_addr: u32, ee_len_addr: u32, buffer: &[u8]) {
    // Write buffer len to eeprom
    EE::write_buf_len(ee, ee_len_addr, buffer.len() as u32, EE::ADDR_SIZE::OneByte).await;
    EE::write_buf(ee, buffer, ee_addr).await;
}

const WEB_TASK_POOL_SIZE: usize = 4;
#[embassy_executor::task(pool_size = WEB_TASK_POOL_SIZE)]
pub async fn web_task(
    stack: &'static Stack<CdcNcmNetDevice<'static, MTU>>,
    mtx_ee: &'static EeMtx,
) -> ! {
    let routers = &picoserve::Router::new()
        .route(
            "/nfctagid",
            post(|picoserve::extract::Form(NfcTag { id })| async move {
                // Convert string to NFC UID bytes array
                let mut tag_id: [u8; 7] = [0; 7];
                for (index, chunk) in id.as_bytes().chunks_exact(2).enumerate() {
                    let hh = hex_to_u8(&[chunk[0], chunk[1]]);
                    tag_id[index] = hh.unwrap();
                }

                init_nfc_tag_id(tag_id).await;

                // Write NFC Tag UID  to eeprom
                EE::write_buf(mtx_deref_mut!(mtx_ee), &tag_id, EE_CFG_ADD::TAGID).await;

                "Saving NFC Tag ID to eeprom completed !!!"
            }),
        )
        .route(
            "/get_nfctagid",
            get(|| async move {
                // Read NFC Tag ID from eeprom

                let mut tag: [u8; 7] = [0; 7];
                EE::read_buf(mtx_deref_mut!(mtx_ee), &mut tag, EE_CFG_ADD::TAGID).await;

                debug!("tag id from eeprom = {:#04X}", tag);

                // Convert to string
                let mut tag_id_str = String::<14>::new();
                for byte in tag {
                    let hh = u8_to_hex_string(byte);
                    tag_id_str.push_str(&hh).unwrap();
                }

                let tag_id = NfcTag { id: tag_id_str };

                picoserve::response::Json(tag_id)
            }),
        )
        .route(
            ("/totp_show_keys", parse_path_segment::<u32>()),
            get(|key_id: u32| async move {
                let totp_count = mtx_deref_mut!(mtx_ee)
                    .read_byte(EE_CFG_ADD::TOTP_COUNT)
                    .unwrap() as u32;

                let mut key = TotpSecretKeyForm {
                    title: String::<14>::try_from("").unwrap(),
                    key: String::<32>::try_from("").unwrap(),
                };

                if !LOCK.load(Ordering::Relaxed)
                    && key_id > 0
                    && key_id <= totp_count
                    && key_id <= 20
                {
                    let key_data = read_totp_from_eeprom(key_id, mtx_ee).await;
                    key.title = key_data.title;
                    key.key = String::<32>::try_from("************************").unwrap();
                }

                picoserve::response::Json(key)
            }),
        )
        .route(
            ("/totp_delete", parse_path_segment::<u32>()),
            get(|key_id: u32| async move {
                info!("Delete Key No = {}", key_id);

                // get the ee device guard
                let mut ee = mtx_ee.lock().await;

                // Copy the following totp data into the eeprom to the empty space created by the deleted element.
                let mut copy_buf: [u8; 16] = [0; 16];
                for n in key_id..20 {
                    EE::read_buf(&mut ee, &mut copy_buf[..], n * 48).await;
                    EE::write_buf(&mut ee, &copy_buf, (n - 1) * 48).await;

                    EE::read_buf(&mut ee, &mut copy_buf[..], n * 48 + 16).await;
                    EE::write_buf(&mut ee, &copy_buf, (n - 1) * 48 + 16).await;

                    EE::read_buf(&mut ee, &mut copy_buf[..], n * 48 + 32).await;
                    EE::write_buf(&mut ee, &copy_buf, (n - 1) * 48 + 32).await;
                }

                let mut totp_count = ee.read_byte(EE_CFG_ADD::TOTP_COUNT).unwrap() as u32;
                totp_count -= 1;
                // Write new totp_count to eeprom
                EE::write_buf_len(
                    &mut ee,
                    EE_CFG_ADD::TOTP_COUNT,
                    totp_count as u32,
                    EE::ADDR_SIZE::OneByte,
                )
                .await;
            }),
        )
        .route(
            "/totp_add",
            post(
                |picoserve::extract::Form(TotpSecretKeyForm { title, key })| async move {
                    if !LOCK.load(Ordering::Relaxed) {
                        debug!("Title = {}", title);
                        debug!("Key  = {} {}", key, key.as_bytes());

                        // get the ee device guard
                        let mut ee = mtx_ee.lock().await;

                        // Read TOTP_COUNT
                        let mut totp_count = ee.read_byte(EE_CFG_ADD::TOTP_COUNT).unwrap();

                        totp_count += 1;
                        let totp_addr = (totp_count as u32 - 1) * 48;

                        debug!("totp_count = {}, totp_addr = {}", totp_count, totp_addr);

                        // Write TOTP Key title to eeprom
                        write_bytes_to_eeprom(&mut ee, totp_addr + 2, totp_addr, title.as_bytes())
                            .await;

                        // Write TOTP key to eeprom
                        let totp_key_len = key.len();
                        let mut buffer: Vec<u8, 32> = Vec::new();
                        buffer.extend_from_slice(key.as_bytes()).unwrap();
                        encrypt_key(&mut buffer[..totp_key_len], &mut ee).await;
                        write_bytes_to_eeprom(
                            &mut ee,
                            totp_addr + 16,
                            totp_addr + 1,
                            &buffer[..totp_key_len],
                        )
                        .await;

                        // Write new TOTP count to eeprom
                        EE::write_buf_len(
                            &mut ee,
                            EE_CFG_ADD::TOTP_COUNT,
                            totp_count as u32,
                            EE::ADDR_SIZE::OneByte,
                        )
                        .await;

                        "Saving TOTP Secret Key to eeprom completed !!!"
                    } else {
                        "No valid NFC Tag found !!!"
                    }
                },
            ),
        )
        .route(
            "/netcfg",
            post(
                |picoserve::extract::Form(NetConfigValue { ssid, pass, ntp })| async move {
                    // get the ee device guard
                    let mut ee = mtx_ee.lock().await;

                    // Write SSID to eeprom
                    write_bytes_to_eeprom(
                        &mut ee,
                        EE_CFG_ADD::SSID + 1,
                        EE_CFG_ADD::SSID,
                        ssid.as_bytes(),
                    )
                    .await;

                    // Write WiFi Password to eeprom
                    write_bytes_to_eeprom(
                        &mut ee,
                        EE_CFG_ADD::WIFIPASS + 1,
                        EE_CFG_ADD::WIFIPASS,
                        pass.as_bytes(),
                    )
                    .await;

                    // Write NTP to eeprom
                    write_bytes_to_eeprom(
                        &mut ee,
                        EE_CFG_ADD::NTP + 1,
                        EE_CFG_ADD::NTP,
                        ntp.as_bytes(),
                    )
                    .await;

                    "Saving options to eeprom completed !!!"
                },
            ),
        )
        .route(
            "/get_netcfg",
            get(|| async move {
                // get the ee device guard
                let mut ee = mtx_ee.lock().await;

                // Read SSID from eeprom
                let mut ssid_len: usize =
                    EE::read_buf_len(&mut ee, EE_CFG_ADD::SSID, EE::ADDR_SIZE::OneByte).await;
                let mut ssid_data: [u8; EE_NET_CFG::LEN] = [0; EE_NET_CFG::LEN];
                if ssid_len <= EE_NET_CFG::LEN {
                    EE::read_buf(&mut ee, &mut ssid_data[..ssid_len], EE_CFG_ADD::SSID + 1).await;
                } else {
                    ssid_len = 0;
                }

                // Read NTP from eeprom
                let mut ntpip_len: usize =
                    EE::read_buf_len(&mut ee, EE_CFG_ADD::NTP, EE::ADDR_SIZE::OneByte).await;
                let mut ntpip_data: [u8; EE_NET_CFG::LEN] = [0; EE_NET_CFG::LEN];
                if ntpip_len <= EE_NET_CFG::LEN {
                    EE::read_buf(&mut ee, &mut ntpip_data[..ntpip_len], EE_CFG_ADD::NTP + 1).await;
                } else {
                    ntpip_len = 0;
                }

                let net_cfg = NetConfigValue {
                    ssid: String::<{ EE_NET_CFG::LEN }>::try_from(
                        str::from_utf8(&ssid_data[..ssid_len]).unwrap(),
                    )
                    .unwrap(),
                    pass: String::<{ EE_NET_CFG::LEN }>::try_from("***************").unwrap(),
                    ntp: String::<{ EE_NET_CFG::LEN }>::try_from(
                        str::from_utf8(&ntpip_data[..ntpip_len]).unwrap(),
                    )
                    .unwrap(),
                };

                picoserve::response::Json(net_cfg)
            }),
        )
        .route(
            "/",
            get(|| picoserve::response::File::html(include_str!("index.html"))),
        );

    let config = make_static!(picoserve::Config::new(picoserve::Timeouts {
        start_read_request: Some(Duration::from_secs(5)),
        read_request: Some(Duration::from_secs(1)),
        write: Some(Duration::from_secs(1)),
    })
    .keep_connection_alive());

    let mut rx_buffer = [0; 1024];
    let mut tx_buffer = [0; 1024];

    loop {
        let mut socket = embassy_net::tcp::TcpSocket::new(stack, &mut rx_buffer, &mut tx_buffer);

        info!("Listening on TCP:80...");
        if let Err(e) = socket.accept(80).await {
            warn!("Accept error: {:?}", e);
            continue;
        }

        info!("Received connection from {:?}", socket.remote_endpoint());

        let (socket_rx, socket_tx) = socket.split();

        match picoserve::serve(
            routers,
            EmbassyTimer,
            config,
            &mut [0; 2048],
            socket_rx,
            socket_tx,
        )
        .await
        {
            Ok(_) => {
                info!("Requests handled from {:?}", socket.remote_endpoint());
            }
            Err(_) => error!("Request error !!! "),
        }
    }
}
