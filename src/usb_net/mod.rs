use defmt::*;
use {defmt_rtt as _, panic_probe as _};

use core::sync::atomic::Ordering;

use embassy_executor::Spawner;
use embassy_net::{Ipv4Address, Ipv4Cidr, Stack, StackResources};
use embassy_sync::mutex::Mutex;
use embassy_usb::class::cdc_ncm::embassy_net::{
    Device as CdcNcmNetDevice, Runner as CdcNcmRunner, State as NetState,
};
use embassy_usb::class::cdc_ncm::{CdcNcmClass, State as CdcNcmState};
use embassy_usb::UsbDevice;

use embassy_rp::gpio::{Level, Output};
use embassy_rp::pio::Pio;
use embassy_rp::{Peripheral, Peripherals};

use heapless::Vec;
use static_cell::make_static;

// My own library
use ee24x08::{self as EE};

// My own module
use crate::nfc::{create_nfc_device, nfc_handle_card, LOCK};
use crate::usb::*;
use crate::usb_net::web_task::web_task;
use crate::ws2812::{blink_led_task, paint_rgb_led, Ws2812, LED_COLOR};
use crate::{unsafe_p_clone, EeMtx, Pio0Irqs};

pub mod web_task;

pub const MTU: usize = 1514;

#[embassy_executor::task]
pub async fn usb_task(mut device: UsbDevice<'static, DriverUsb>) -> ! {
    device.run().await
}

#[embassy_executor::task]
pub async fn usb_ncm_task(class: CdcNcmRunner<'static, DriverUsb, MTU>) -> ! {
    class.run().await
}

fn create_usb_net_task(
    spawner: &Spawner,
    p: &mut Peripherals,
    our_mac_addr: [u8; 6],
    host_mac_addr: [u8; 6],
) -> CdcNcmNetDevice<'static, MTU> {
    // Create embassy-usb DeviceBuilder using the driver and config.
    let mut usb_builder = create_usb_builder(p, UsbMode::CDC);

    usb_builder.function(0x03, 0x00, 0x00); // Set HID type to default

    // Create classes on the builder.
    let usb_class = CdcNcmClass::new(
        &mut usb_builder,
        make_static!(CdcNcmState::new()),
        host_mac_addr,
        64,
    );

    // Build the builder.
    let usb = usb_builder.build();

    unwrap!(spawner.spawn(usb_task(usb)));

    let (runner, net_device) =
        usb_class.into_embassy_net_device::<MTU, 4, 4>(make_static!(NetState::new()), our_mac_addr);
    unwrap!(spawner.spawn(usb_ncm_task(runner)));

    net_device
}

#[embassy_executor::task]
pub async fn net_task(stack: &'static Stack<CdcNcmNetDevice<'static, MTU>>) -> ! {
    stack.run().await
}

pub async fn usb_net(spawner: &Spawner, p: &mut Peripherals) {
    // Create pio config for wifi
    let pio = Pio::new(unsafe_p_clone!(p.PIO0), Pio0Irqs);
    let mut pio_common = pio.common;
    let pio_sm3 = pio.sm3;

    // Initialise the Ws2812 LED
    //
    let led = make_static!(Ws2812::new(
        &mut pio_common,
        pio_sm3,
        unsafe_p_clone!(p.DMA_CH3),
        unsafe_p_clone!(p.PIN_11)
    ));
    unwrap!(spawner.spawn(blink_led_task(led)));

    // Our internal MAC addr.
    let our_mac_addr = [0xCC, 0xCC, 0xCC, 0xCC, 0xCC, 0xCC];

    // Host's MAC addr. This is the MAC the host "thinks" its USB-to-ethernet adapter has.
    let host_mac_addr = [0x88, 0x88, 0x88, 0x88, 0x88, 0x88];

    // Start the USB net tasks
    let net_device = create_usb_net_task(spawner, p, our_mac_addr, host_mac_addr);

    //let config = embassy_net::Config::dhcpv4(Default::default());
    let net_config = embassy_net::Config::ipv4_static(embassy_net::StaticConfigV4 {
        address: Ipv4Cidr::new(Ipv4Address::new(169, 254, 123, 123), 16),
        dns_servers: Vec::new(),
        gateway: None,
    });

    // Generate random seed
    let seed = 29384729; // guaranteed random, chosen by a fair dice roll

    // Init network stack
    let stack = &*make_static!(Stack::new(
        net_device,
        net_config,
        make_static!(StackResources::<2>::new()),
        seed
    ));

    // Spawn network task
    unwrap!(spawner.spawn(net_task(stack)));

    // Create static Mutex on the 24x08 eeprom device
    let mtx_ee: &mut EeMtx = make_static!(Mutex::new(EE::create_24x08_eeprom(p)));

    // Spawn web config task port 80
    unwrap!(spawner.spawn(web_task(stack, mtx_ee)));

    // NFC
    // Mini module reset high first
    let mut reset_pin = Output::new(unsafe_p_clone!(p.PIN_2), Level::High);
    reset_pin.set_high();
    let mfrc522 = create_nfc_device(
        unsafe_p_clone!(p.SPI0),    // SPI Controler 0
        unsafe_p_clone!(p.PIN_5),   // cs
        unsafe_p_clone!(p.PIN_6),   // clk
        unsafe_p_clone!(p.PIN_7),   // mosi
        unsafe_p_clone!(p.PIN_4),   // miso
        unsafe_p_clone!(p.DMA_CH1), // dma ch1
        unsafe_p_clone!(p.DMA_CH2), // dma ch2
    );

    // Bug: Unable to create nfc_task in usb_net mode, look like
    //      the task freeze USB when in CDC-NCM mode
    // unwrap!(spawner.spawn(nfc_task(mfrc522, mtx_ee, 0)));
    //
    // Actualy using a loop is ok there
    loop {
        nfc_handle_card(mfrc522, mtx_ee, 0).await;

        // Manage LED color
        if !LOCK.load(Ordering::Relaxed) {
            paint_rgb_led(LED_COLOR::GREEN).await;
        } else {
            paint_rgb_led(LED_COLOR::ORANGE).await;
        }
    }
}
