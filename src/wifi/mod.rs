use defmt::*;

use embassy_executor::Spawner;

use embassy_rp::gpio::{Level, Output};
use embassy_rp::{Peripheral, Peripherals};

use static_cell::make_static;

use {
    cyw43::Control,
    cyw43_pio::PioSpi,
    embassy_net_driver_channel::Device as WiFiNetChannelDevice,
    embassy_rp::peripherals::{DMA_CH0, PIO0},
    embassy_rp::pio::{Common as PioCommon, Irq as PioIrq, StateMachine as PioStateMachine},
};

use crate::unsafe_p_clone;

#[embassy_executor::task]
pub async fn wifi_task(
    runner: cyw43::Runner<'static, Output<'static>, PioSpi<'static, PIO0, 0, DMA_CH0>>,
) -> ! {
    runner.run().await
}

pub async fn initialize_wifi(
    spawner: &Spawner,
    p: &mut Peripherals,
    pio_common: &mut PioCommon<'static, PIO0>,
    pio_sm0: PioStateMachine<'static, PIO0, 0>,
    pio_irq0: PioIrq<'static, PIO0, 0>,
) -> (WiFiNetChannelDevice<'static, 1514>, Control<'static>) {
    // WiFi cyw43 setup
    //
    let fw = include_bytes!("../../../../embassy/cyw43-firmware/43439A0.bin");
    let clm = include_bytes!("../../../../embassy/cyw43-firmware/43439A0_clm.bin");

    // To make flashing faster for development, you may want to flash the firmwares independently
    // at hardcoded addresses, instead of baking them into the program with `include_bytes!`:
    //     probe-rs download 43439A0.bin --format bin --chip RP2040 --base-address 0x10100000
    //     probe-rs download 43439A0_clm.bin --format bin --chip RP2040 --base-address 0x10140000
    //let fw = unsafe { core::slice::from_raw_parts(0x10100000 as *const u8, 224190) };
    //let clm = unsafe { core::slice::from_raw_parts(0x10140000 as *const u8, 4752) };

    let pwr = Output::new(unsafe_p_clone!(p.PIN_23), Level::Low);
    let cs = Output::new(unsafe_p_clone!(p.PIN_25), Level::High);

    let spi = PioSpi::new(
        pio_common,
        pio_sm0,
        pio_irq0,
        cs,
        unsafe_p_clone!(p.PIN_24),
        unsafe_p_clone!(p.PIN_29),
        unsafe_p_clone!(p.DMA_CH0),
    );

    let state = make_static!(cyw43::State::new());
    let (net_device, mut control, runner) = cyw43::new(state, pwr, spi, fw).await;
    unwrap!(spawner.spawn(wifi_task(runner)));

    control.init(clm).await;
    control
        .set_power_management(cyw43::PowerManagementMode::PowerSave)
        .await;

    (net_device, control)
}
