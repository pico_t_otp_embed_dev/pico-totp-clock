use heapless::Vec;

use chacha20::cipher::{KeyIvInit, StreamCipher, StreamCipherSeek};
use chacha20::{Key, XChaCha20, XNonce};

use ee24x08::{self as EE, Ee24x08};

use crate::{get_cc20_key, get_serial_number, CC20_NONCE, EE_CFG_ADD};

pub const FLASH_SIZE: usize = 2 * 1024 * 1024;

// Create XNonce
async fn get_xnonce(ee: &mut Ee24x08) -> [u8; 24] {
    let mut xnonce: Vec<u8, 24> = Vec::new();
    let mut xn1: [u8; 4] = [0; 4];
    EE::read_buf(ee, &mut xn1[..], EE_CFG_ADD::TAGID + 3).await;
    xnonce.extend_from_slice(&xn1).unwrap();
    xnonce
        .extend_from_slice(&get_serial_number().await)
        .unwrap();
    xnonce.extend_from_slice(CC20_NONCE).unwrap();
    xnonce.into_array().unwrap()
}

pub async fn decrypt_key(key: &mut [u8], ee: &mut Ee24x08) {
    // XChaCha20 decrypt key
    let cipher = &mut XChaCha20::new(
        Key::from_slice(&get_cc20_key().await),
        XNonce::from_slice(&get_xnonce(ee).await),
    );
    cipher.seek(0u32);
    cipher.apply_keystream(&mut key[..]);
}

pub async fn encrypt_key(key: &mut [u8], ee: &mut Ee24x08) {
    // XChaCha20 encrypt key
    let cipher = &mut XChaCha20::new(
        Key::from_slice(&get_cc20_key().await),
        XNonce::from_slice(&get_xnonce(ee).await),
    );
    cipher.apply_keystream(&mut key[..]);
}
