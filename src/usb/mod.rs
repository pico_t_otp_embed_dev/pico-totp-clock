use defmt::*;

use core::sync::atomic::{AtomicBool, Ordering};

use embassy_rp::peripherals::{self, USB};
use embassy_rp::usb::{Driver, InterruptHandler as UsbInterruptHandler};
use embassy_rp::{bind_interrupts, Peripheral, Peripherals};

use embassy_usb::class::hid::{ReportId, RequestHandler};
use embassy_usb::control::OutResponse;
use embassy_usb::{Builder as UsbBuilder, Config as UsbConfig, Handler};

use static_cell::make_static;

pub enum UsbMode {
    HID,
    CDC,
}

use crate::unsafe_p_clone;

pub const USB_VENDOR_ID: u16 = 0xC0DE;
pub const USB_PRODUCT_ID: u16 = 0xCAFE;

pub type DriverUsb = Driver<'static, peripherals::USB>;

bind_interrupts!(pub struct UsbIrqs {
    USBCTRL_IRQ => UsbInterruptHandler<USB>;
});

pub fn create_usb_builder(
    p: &mut Peripherals,
    usb_mode: UsbMode,
) -> UsbBuilder<'static, DriverUsb> {
    // Create the driver, from the HAL.
    let usb_driver = Driver::new(unsafe_p_clone!(p.USB), UsbIrqs);

    let mut usb_config = UsbConfig::new(USB_VENDOR_ID, USB_PRODUCT_ID);
    usb_config.manufacturer = Some("Ivan Arsenault");
    usb_config.product = Some("Totp Clock");
    usb_config.serial_number = None;
    usb_config.device_release = 0x104; // Ex: bcdDevice = 1.04
    usb_config.max_power = 100;
    usb_config.max_packet_size_0 = 64;

    match usb_mode {
        UsbMode::HID => {
            // Required for Windows support.
            usb_config.composite_with_iads = true;
            usb_config.device_class = 0xEF;
            usb_config.device_sub_class = 0x02;
            usb_config.device_protocol = 0x01;
        }
        UsbMode::CDC => {
            // Required for Windows support.
            usb_config.composite_with_iads = false;
            usb_config.device_class = 0xEF;
            usb_config.device_sub_class = 0x04;
            usb_config.device_protocol = 0x01;
        }
    }

    // Create embassy-usb DeviceBuilder using the driver and config.
    let usb_builder = UsbBuilder::new(
        usb_driver,
        usb_config,
        &mut make_static!([0; 256])[..], // config descriptor
        &mut make_static!([0; 256])[..], // bos descriptor
        &mut make_static!([0; 256])[..], // msos descriptor
        &mut make_static!([0; 128])[..], // control buffer
    );

    usb_builder
}

pub struct UsbRequestHandler {}

impl RequestHandler for UsbRequestHandler {
    fn get_report(&mut self, id: ReportId, _buf: &mut [u8]) -> Option<usize> {
        info!("USB: Get report for {:?}", id);
        None
    }

    fn set_report(&mut self, id: ReportId, data: &[u8]) -> OutResponse {
        info!("USB: Set report for {:?}: {=[u8]}", id, data);
        OutResponse::Accepted
    }

    fn set_idle_ms(&mut self, id: Option<ReportId>, dur: u32) {
        info!("USB: Set idle rate for {:?} to {:?}", id, dur);
    }

    fn get_idle_ms(&mut self, id: Option<ReportId>) -> Option<u32> {
        info!("USB: Get idle rate for {:?}", id);
        None
    }
}

pub struct UsbDeviceHandler {
    configured: AtomicBool,
}

impl UsbDeviceHandler {
    pub fn new() -> Self {
        UsbDeviceHandler {
            configured: AtomicBool::new(false),
        }
    }
}

impl Handler for UsbDeviceHandler {
    fn enabled(&mut self, enabled: bool) {
        self.configured.store(false, Ordering::Relaxed);
        if enabled {
            info!("USB: Device enabled");
        } else {
            info!("USB: Device disabled");
        }
    }

    fn reset(&mut self) {
        self.configured.store(false, Ordering::Relaxed);
        info!("USB: Bus reset, the Vbus current limit is 100mA");
    }

    fn addressed(&mut self, addr: u8) {
        self.configured.store(false, Ordering::Relaxed);
        info!("USB: Address set to: {}", addr);
    }

    fn configured(&mut self, configured: bool) {
        self.configured.store(configured, Ordering::Relaxed);
        if configured {
            info!(
                "USB: Device configured, it may now draw up to the configured current limit from Vbus."
            )
        } else {
            info!("USB: Device is no longer configured, the Vbus current limit is 100mA.");
        }
    }
}
