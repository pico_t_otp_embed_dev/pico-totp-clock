use defmt::*;
use {defmt_rtt as _, panic_probe as _};

use core::ops::DerefMut;
use core::task::Poll;

use embassy_futures::poll_once;
use embassy_net::{udp::UdpSocket, IpAddress, IpEndpoint};
use embassy_sync::{blocking_mutex::raw::NoopRawMutex, mutex::Mutex};
use embassy_time::{Duration, Ticker};

use embassy_rp::peripherals::RTC;
use embassy_rp::rtc::Rtc;

use arbitrary_int::u3;
use chrono::{TimeZone, Utc};
use chrono_tz::America::Montreal;

// My own module
use crate::mtx_deref_mut;
use crate::sntp::{LeapIndicator, ProtocolMode, Stratum, Timestamp, LVMSPP, SNTP};
use crate::ws2812::{blink_rgb_led, paint_rgb_led, LED_COLOR};

pub type RtcMtx = Mutex<NoopRawMutex, Rtc<'static, RTC>>;

#[embassy_executor::task]
pub async fn rtc_task(
    socket: &'static UdpSocket<'_>,
    rtc: &'static RtcMtx,
    ntp_server_addr: IpAddress,
) {
    // update RTC each 4hours
    #[allow(unused_assignments)]
    let mut ticker = Ticker::every(Duration::from_secs(4 * 60 * 60));

    loop {
        // Construc UDP NTP request packet
        let sntp_client_packet = SNTP {
            lvmspp: LVMSPP::builder()
                .with_leap_indicator(LeapIndicator::NoWarning.raw_value())
                .with_version(u3::new(4))
                .with_protocol_mode(ProtocolMode::Client.raw_value())
                .with_stratum(Stratum::KissOfDeath.into())
                .with_pool_interval(0)
                .with_precision(0)
                .build(),
            root_delay: 0,
            root_dispersion: 0,
            ref_identifier: [0, 0, 0, 0],
            ref_timestamp: Timestamp::DEFAULT,
            orig_timestamp: Timestamp::DEFAULT,
            recv_timestamp: Timestamp::DEFAULT,
            xmit_timestamp: Timestamp::DEFAULT,
        }
        .to_bytes();

        // Sends a NTP cient request to the configured SNTP ntp_server
        let ntp_server_ep = IpEndpoint::new(ntp_server_addr, 123);
        let res = socket.send_to(&sntp_client_packet, ntp_server_ep).await;

        match res {
            Ok(()) => (),
            Err(e) => {
                error!("RTC: ntp send_to error !!! {}", e)
            }
        }

        let mut buf = [0; 4096];

        Ticker::every(Duration::from_millis(1000)).next().await; // Wait 1 sec to received UDP NTP response

        let fut = socket.recv_from(&mut buf);

        // Poll future to get NTP data
        let data_size = match poll_once(fut) {
            Poll::Ready(n) => n.unwrap().0,
            Poll::Pending => {
                error!("RTC: ntp recv_from error !!!");
                0
            }
        };

        if data_size > 0 {
            // if no bytes received skip RTC setup

            info!("RTC: ----  UPDATE RTC FROM NTP !!! ----");
            let sntp_recieved_packet = SNTP::from_bytes(&buf[..data_size]).unwrap();
            // Convert received SNTP packet to Montreal local_datetime
            const DIFF_SEC_1970_2036: u32 = 2_085_978_496; // To adjust epoch to get unix_timestamp
                                                           // Add diff From 1 January 1900 to 1 January 1970 UTC
            let unix_timestamp = sntp_recieved_packet
                .recv_timestamp
                .sec()
                .wrapping_add(DIFF_SEC_1970_2036);
            let utc_datetime = Utc.timestamp_opt(unix_timestamp as i64, 0).unwrap();
            let local_datetime = Montreal.from_utc_datetime(&utc_datetime.naive_local());

            // Update RTC with received data
            mtx_deref_mut!(rtc)
                .set_datetime(local_datetime.naive_local())
                .unwrap();

            paint_rgb_led(LED_COLOR::ORANGE).await;
            ticker = Ticker::every(Duration::from_secs(4 * 60 * 60)); // Network is ok so try each 4hours
        } else {
            blink_rgb_led(2, LED_COLOR::ORANGE, LED_COLOR::OFF).await;
            ticker = Ticker::every(Duration::from_secs(5 * 60)); // Maybe WiFi is not avaiable or NTP serveur is not available so try each 5min.
        }
        ticker.next().await;
    }
}
