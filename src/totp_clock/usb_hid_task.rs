use core::sync::atomic::Ordering;

use embassy_futures::join::join;
use embassy_time::{Duration, Timer};
use embassy_usb::class::hid::{HidReaderWriter, State as HidState};
use usbd_hid::descriptor::{KeyboardReport, SerializedDescriptor};

use embassy_rp::Peripherals;
use static_cell::make_static;

// My own library
use kbd::{kbd_us::UsKbd, Keyboard};

// My own module
use crate::leftpad_u32_to_string;
use crate::rotenc::{COUNT, SWITCH_PRESSED};
use crate::totp_clock::TOTP;
use crate::usb::{create_usb_builder, UsbDeviceHandler, UsbMode, UsbRequestHandler};

// USB HID Task
//
pub async fn handle_usb_task(p: &mut Peripherals) {
    // Create embassy-usb DeviceBuilder
    let mut usb_builder = create_usb_builder(p, UsbMode::HID);

    // Attach device handler call back
    let device_handler = make_static!(UsbDeviceHandler::new());
    usb_builder.handler(device_handler);

    let request_handler = make_static!(UsbRequestHandler {});
    let hid_state: &mut HidState<'_> = make_static!(HidState::new());

    // Create classes on the builder.
    let hid_config = embassy_usb::class::hid::Config {
        report_descriptor: KeyboardReport::desc(),
        request_handler: None,
        poll_ms: 10,
        max_packet_size: 64,
    };

    let hid = HidReaderWriter::<_, 1, 8>::new(&mut usb_builder, hid_state, hid_config);

    // Build the usb from builder
    let mut usb = usb_builder.build();

    // Run the USB device.
    let usb_fut = usb.run();

    // Split to hid reader and writer
    let (reader, mut writer) = hid.split();

    let kbd = Keyboard::Us(UsKbd);

    let in_fut = async {
        loop {
            if COUNT.load(Ordering::Relaxed) > 0 && SWITCH_PRESSED.load(Ordering::Acquire) {
                kbd.str_to_keyboard(
                    &mut writer,
                    &leftpad_u32_to_string::<6>(TOTP.load(Ordering::Relaxed), '0'),
                )
                .await;
                SWITCH_PRESSED.store(false, Ordering::Release);
                COUNT.store(0, Ordering::Relaxed);
            }
            Timer::after(Duration::from_millis(10)).await;
        }
    };

    let out_fut = async {
        reader.run(false, request_handler).await;
    };

    join(usb_fut, join(in_fut, out_fut)).await;
}
