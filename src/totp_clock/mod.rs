use defmt::*;
use portable_atomic::Ordering;
use {defmt_rtt as _, panic_probe as _};

use core::{
    ops::DerefMut,
    str,
    sync::atomic::{AtomicBool, AtomicU32},
};

use embassy_executor::Spawner;
use embassy_net::{
    udp::{PacketMetadata, UdpSocket},
    Config as NetConfig, HardwareAddress, IpAddress, Stack, StackResources, StaticConfigV4,
};
use embassy_sync::mutex::Mutex;
use embassy_time::{Duration, Timer};

use embassy_rp::bind_interrupts;
use embassy_rp::i2c::{self, Config as I2cConfig, InterruptHandler as I2cInterruptHandler};
use embassy_rp::peripherals::{DMA_CH0, I2C0, PIO0};
use embassy_rp::pio;
use embassy_rp::rtc::Rtc;
use embassy_rp::{gpio, Peripheral, Peripherals};

use cyw43_pio::PioSpi;
use pio::Pio;

use gpio::{Level, Output};
use heapless::{String, Vec};
use ssd1306::{prelude::*, I2CDisplayInterface, Ssd1306};
use static_cell::make_static;

// My own library
use ee24x08::{self as EE};

// My own module
use crate::display::print_info;
use crate::enc::decrypt_key;
use crate::nfc::{create_nfc_device, nfc_task};
use crate::rotenc::{encoder_count_task, encoder_switch_task, PioEncoder, PioSwitch};
use crate::totp_clock::display_tasks::{
    display_clock_task, display_totp_task, display_wait_anim_task, DisplayMtx,
};
use crate::totp_clock::rtc_task::{rtc_task, RtcMtx};
use crate::totp_clock::usb_hid_task::handle_usb_task;
use crate::wifi::initialize_wifi;
use crate::ws2812::{blink_led_task, blink_rgb_led, Ws2812, LED_COLOR};
use crate::{
    mtx_deref_mut, u8_to_hex_string, unsafe_p_clone, EeMtx, Pio0Irqs, TotpSecretKey, EE_CFG_ADD,
    EE_NET_CFG, VERSION,
};

pub mod display_tasks;
pub mod rtc_task;
pub mod usb_hid_task;

pub static TOTP: AtomicU32 = AtomicU32::new(0);
pub static WAITING: AtomicBool = AtomicBool::new(true);

bind_interrupts!(struct I2cIrqs {
    I2C0_IRQ => I2cInterruptHandler<I2C0>;
});

#[embassy_executor::task]
async fn wifi_task(
    runner: cyw43::Runner<'static, Output<'static>, PioSpi<'static, PIO0, 0, DMA_CH0>>,
) -> ! {
    runner.run().await
}

#[embassy_executor::task]
async fn net_task(stack: &'static Stack<cyw43::NetDriver<'static>>) -> ! {
    stack.run().await
}

fn ipv4_address_to_string(scv4: StaticConfigV4) -> String<15> {
    let mut formatted_ipv4_address: String<15> = String::new();

    for (i, &ip) in scv4.address.address().0.iter().enumerate() {
        formatted_ipv4_address
            .push_str(&String::<15>::try_from(ip).unwrap())
            .unwrap();
        if i < 3 {
            formatted_ipv4_address.push_str(".").unwrap();
        }
    }

    formatted_ipv4_address
}

fn hardware_address_to_string(ha: HardwareAddress) -> String<17> {
    let mut s_ha: String<17> = String::new();

    for (i, &mac) in ha.as_bytes().iter().enumerate() {
        let _ = s_ha.push_str(&u8_to_hex_string(mac));
        if i < 5 {
            let _ = s_ha.push_str(":");
        }
    }

    s_ha
}

// Read n bytes from eeprom
async fn read_bytes_from_eeprom<U8, const N: usize>(
    mtx_ee: &'static EeMtx,
    ee_len_addr: u32,
    ee_addr: u32,
) -> Vec<u8, { N }> {
    let mut result: Vec<u8, { N }> = Vec::new();

    // Read size from eeprom
    let r_len: usize =
        EE::read_buf_len(mtx_deref_mut!(mtx_ee), ee_len_addr, EE::ADDR_SIZE::OneByte).await;

    let _ = result.resize_default(r_len);

    // Read data from eeprom
    EE::read_buf(mtx_deref_mut!(mtx_ee), &mut result[..r_len], ee_addr).await;

    result
}

pub async fn read_totp_from_eeprom(id: u32, mtx_ee: &'static EeMtx) -> TotpSecretKey {
    // Calculate eeprom key addr
    let totp_ee_addr = (id - 1) * 48;

    let title: Vec<u8, 14> =
        read_bytes_from_eeprom::<u8, 14>(mtx_ee, totp_ee_addr, totp_ee_addr + 2).await;
    let title = str::from_utf8(&title).unwrap();

    let mut totp_key: Vec<u8, 32> =
        read_bytes_from_eeprom::<u8, 32>(mtx_ee, totp_ee_addr + 1, totp_ee_addr + 16).await;
    decrypt_key(&mut totp_key, mtx_deref_mut!(mtx_ee)).await;

    TotpSecretKey {
        title: String::<14>::try_from(title).unwrap(),
        key: totp_key,
    }
}

// A simple function to convert a byte char to its numerical value
fn byte_to_digit(byte: u8) -> Option<u8> {
    match byte {
        b'0'..=b'9' => Some(byte - b'0'),
        _ => None,
    }
}

// A function to convert ntp ipv4 string bytes to an Ipv4Adress
fn ntp_bytes_to_ip_address(bytes: &[u8]) -> Option<IpAddress> {
    let mut v4: Vec<u8, 4> = Vec::new();

    // Closure tu push value to v4 vec with convertion of Error to None
    let mut push_a_error = None;
    let mut push_a = |a| {
        if let Err(e) = v4.push(a) {
            push_a_error = Some(e);
        }
    };

    let mut a = 0;
    for &b in bytes.iter() {
        match byte_to_digit(b) {
            Some(digit) => {
                // Is a digit
                a = a * 10 + digit;
            }
            None => {
                // If None is the "."
                push_a(a);
                a = 0;
            }
        }
    }
    push_a(a);

    // If error with vec v4.push return None
    if let Some(e) = push_a_error {
        error!(
            "Convert to IpV4Address error on ntp_bytes_to_ip_address !!! {}",
            e,
        );
        return None;
    }

    Some(IpAddress::v4(v4[0], v4[1], v4[2], v4[3]))
}

async fn infinite_led_blink(nb_blink: u8) {
    // Infinite 3 blink to show WiFi join error
    blink_rgb_led(nb_blink, LED_COLOR::RED, LED_COLOR::OFF).await;
    loop {
        Timer::after(Duration::from_millis(1000)).await;
    }
}

pub async fn totp_clock(spawner: &Spawner, p: &mut Peripherals) {
    // Create pio and is needed config for wifi and rotary encoder
    let pio = Pio::new(unsafe_p_clone!(p.PIO0), Pio0Irqs);
    let mut pio_common = pio.common; // Used by wifi and rotary encoder
    let pio_sm0 = pio.sm0; // Used by wifi
    let pio_sm1 = pio.sm1; // Used by rotary encoder
    let pio_sm2 = pio.sm2; // Used by rotary encoder switch
    let pio_sm3 = pio.sm3; // Used by ws2812 RGB led
    let pio_irq0 = pio.irq0; // Used by wifi

    // Initialise the Ws2812 LED
    //
    let led = make_static!(Ws2812::new(
        &mut pio_common,
        pio_sm3,
        unsafe_p_clone!(p.DMA_CH3),
        unsafe_p_clone!(p.PIN_11)
    ));
    unwrap!(spawner.spawn(blink_led_task(led)));

    // Setup i2c for the display
    //
    let sda = unsafe_p_clone!(p.PIN_12);
    let scl = unsafe_p_clone!(p.PIN_13);
    let mut i2c_config = I2cConfig::default();
    i2c_config.frequency = 400_000;
    let i2c = i2c::I2c::new_async(unsafe_p_clone!(p.I2C0), scl, sda, I2cIrqs, i2c_config);
    let i2c_display_interface = I2CDisplayInterface::new(i2c);

    // Create the display device on a static Mutex
    let display: &mut DisplayMtx = make_static!(Mutex::new(
        Ssd1306::new(
            i2c_display_interface,
            DisplaySize128x64,
            DisplayRotation::Rotate0,
        )
        .into_buffered_graphics_mode()
    ));

    // Init display
    mtx_deref_mut!(display).init().unwrap();

    // Task that display waiting animation
    unwrap!(spawner.spawn(display_wait_anim_task(display)));
    Timer::after(Duration::from_millis(500)).await;

    //  Initialise WiFi device and create the network stack
    //
    let (wifi_net_device, mut wifi_control) =
        initialize_wifi(spawner, p, &mut pio_common, pio_sm0, pio_irq0).await;

    wifi_control
        .set_power_management(cyw43::PowerManagementMode::PowerSave)
        .await;

    let net_config = NetConfig::dhcpv4(Default::default());

    // Generate random seed
    let seed = 0x0123_4567_89ab_cdef; // chosen by fair dice roll. guarenteed to be random.

    // Init network stack
    let stack = &*make_static!(Stack::new(
        wifi_net_device,
        net_config,
        make_static!(StackResources::<2>::new()),
        seed,
    ));

    unwrap!(spawner.spawn(net_task(stack)));
    // Done WiFi and stack

    // Create the 24x08 eeprom device in a static Mutex
    let mtx_ee: &mut EeMtx = make_static!(Mutex::new(EE::create_24x08_eeprom(p)));

    // Read eeprom to get max number of recorded totp secret key
    let max_count =
        EE::read_buf_len(mtx_deref_mut!(mtx_ee), 1023, EE::ADDR_SIZE::OneByte).await as u32;

    // Read SSID from eeprom
    let ssid: Vec<u8, { EE_NET_CFG::LEN }> = read_bytes_from_eeprom::<u8, { EE_NET_CFG::LEN }>(
        mtx_ee,
        EE_CFG_ADD::SSID,
        EE_CFG_ADD::SSID + 1,
    )
    .await;

    // Read WIFI password from eeprom
    let wifi_pass: Vec<u8, { EE_NET_CFG::LEN }> =
        read_bytes_from_eeprom::<u8, { EE_NET_CFG::LEN }>(
            mtx_ee,
            EE_CFG_ADD::WIFIPASS,
            EE_CFG_ADD::WIFIPASS + 1,
        )
        .await;

    // Read NTP IP from eeprom
    let ntp_ip_addr: Vec<u8, { EE_NET_CFG::LEN }> =
        read_bytes_from_eeprom::<u8, { EE_NET_CFG::LEN }>(
            mtx_ee,
            EE_CFG_ADD::NTP,
            EE_CFG_ADD::NTP + 1,
        )
        .await;
    let ntp_server_addr = ntp_bytes_to_ip_address(&ntp_ip_addr).unwrap();

    // Try join WiFi
    loop {
        match wifi_control
            .join_wpa2(
                str::from_utf8(&ssid).unwrap(),
                str::from_utf8(&wifi_pass).unwrap(),
            )
            .await
        {
            Ok(_) => break,
            Err(err) => {
                info!("Join failed with status = {}", err.status);

                // Infinite 3 blink to show WiFi join error
                infinite_led_blink(3).await;
            }
        }
    }

    // Wait for DHCP, not necessary when using static IP
    info!("Waiting for DHCP...");
    let mut nb_dhcp_try = 100;
    while !stack.is_config_up() {
        Timer::after_millis(200).await;
        if nb_dhcp_try == 0 {
            info!("DHCP timeout !!!");

            // Infinite Red 5 blink to show DCHP error
            infinite_led_blink(5).await;
        }
        nb_dhcp_try -= 1;
    }

    // And now we can use it!
    info!("DHCP is now up !");

    let socket = &mut *make_static!(UdpSocket::new(
        stack,
        make_static!([PacketMetadata::EMPTY; 16]),
        make_static!([0; 4096]),
        make_static!([PacketMetadata::EMPTY; 16]),
        make_static!([0; 4096]),
    ));

    let bind_result = socket.bind(0);
    if let Err(e) = bind_result {
        // Handle socket bind error
        error!("Socket bind error = {}", e);
    }
    //
    // End WiFi & Net Task

    // Create a static Mutex with the rtc device
    let mtx_rtc: &mut RtcMtx = make_static!(Mutex::new(Rtc::new(unsafe_p_clone!(p.RTC))));

    unwrap!(spawner.spawn(rtc_task(socket, mtx_rtc, ntp_server_addr)));

    // Waiting is done
    WAITING.store(false, Ordering::Relaxed);
    // Display WiFI info 5sec
    let mac_addr = hardware_address_to_string(stack.hardware_address());
    let ipv4_addr = ipv4_address_to_string(stack.config_v4().unwrap());
    print_info(
        mtx_deref_mut!(display),
        &String::try_from(VERSION).unwrap(),
        &mac_addr,
        &ipv4_addr,
    );
    Timer::after(Duration::from_millis(5000)).await;
    unwrap!(spawner.spawn(display_clock_task(mtx_rtc, display)));
    unwrap!(spawner.spawn(display_totp_task(mtx_rtc, display)));

    // Rotary encoder
    //
    let encoder = &mut *make_static!(PioEncoder::new(
        &mut pio_common,
        pio_sm1,
        unsafe_p_clone!(p.PIN_16),
        unsafe_p_clone!(p.PIN_17)
    ));
    let switch = &mut *make_static!(PioSwitch::new(
        &mut pio_common,
        pio_sm2,
        unsafe_p_clone!(p.PIN_18)
    ));
    unwrap!(spawner.spawn(encoder_count_task(encoder, max_count))); // Limit encoder to 0..max_count
    unwrap!(spawner.spawn(encoder_switch_task(switch)));

    // NFC
    // Mini module reset high first
    let mut reset_pin = Output::new(unsafe_p_clone!(p.PIN_2), Level::High);
    reset_pin.set_high();

    let mfrc522 = create_nfc_device(
        unsafe_p_clone!(p.SPI0),    // SPI Controler 0
        unsafe_p_clone!(p.PIN_5),   // cs
        unsafe_p_clone!(p.PIN_6),   // clk
        unsafe_p_clone!(p.PIN_7),   // mosi
        unsafe_p_clone!(p.PIN_4),   // miso
        unsafe_p_clone!(p.DMA_CH1), // dma ch1
        unsafe_p_clone!(p.DMA_CH2), // dma ch2
    );
    unwrap!(spawner.spawn(nfc_task(mfrc522, mtx_ee, max_count)));

    // USB HID Task
    handle_usb_task(p).await;
}
