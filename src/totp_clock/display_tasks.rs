use core::ops::DerefMut;
use core::sync::atomic::Ordering;

use embassy_sync::{blocking_mutex::raw::NoopRawMutex, mutex::Mutex};
use embassy_time::{Duration, Ticker};

use binascii::b32decode;
use chrono::{Datelike, Timelike};
use chrono_tz::America::Montreal;
use totp_embed::{totp_custom, Sha1, DEFAULT_STEP};

// My own module
use crate::display::{print_clock, print_dot, print_totp, Display128x64};
use crate::leftpad_u32_to_string;
use crate::nfc::LOCK;
use crate::rotenc::COUNT;
use crate::totp_clock::{rtc_task::RtcMtx, TOTP, WAITING};
use crate::{mtx_deref_mut, TOTP_SK_LIST};

pub type DisplayMtx = Mutex<NoopRawMutex, Display128x64>;

#[embassy_executor::task]
pub async fn display_wait_anim_task(display: &'static DisplayMtx) {
    let mut ticker = Ticker::every(Duration::from_millis(80));
    let mut nb_dot = 0;
    let mut increasing = true;
    loop {
        if WAITING.load(Ordering::Relaxed) {
            print_dot(mtx_deref_mut!(display), nb_dot);

            nb_dot = match (nb_dot, increasing) {
                (0, false) => {
                    increasing = true;
                    1
                }
                (3, true) => {
                    increasing = false;
                    2
                }
                (_, true) => nb_dot + 1,
                (_, false) => nb_dot - 1,
            };
        } else {
            ticker = Ticker::every(Duration::from_secs(31_536_000));
        }
        ticker.next().await;
    }
}

#[embassy_executor::task]
pub async fn display_clock_task(mtx_rtc: &'static RtcMtx, display: &'static DisplayMtx) {
    let mut ticker = Ticker::every(Duration::from_micros(62500));

    loop {
        if COUNT.load(Ordering::Relaxed) == 0 {
            if let Ok(ndt) = mtx_deref_mut!(mtx_rtc).now() {
                print_clock(
                    mtx_deref_mut!(display),
                    ndt.hour(),
                    ndt.minute(),
                    ndt.second(),
                    ndt.year(),
                    ndt.month(),
                    ndt.day(),
                );
            }
        }
        ticker.next().await;
    }
}

#[embassy_executor::task]
pub async fn display_totp_task(mtx_rtc: &'static RtcMtx, display: &'static DisplayMtx) {
    let mut ticker = Ticker::every(Duration::from_micros(62500));

    // Display actual totp
    loop {
        let count = COUNT.load(Ordering::Relaxed);
        if count > 0 && !LOCK.load(Ordering::Relaxed) {
            if let Ok(native_date_time) = mtx_deref_mut!(mtx_rtc).now() {
                let t_sk_l = &TOTP_SK_LIST.lock().await;
                let title = &t_sk_l.data[(count - 1) as usize].title;

                // Get utc unix timestamp
                let unix_timestamp = native_date_time
                    .and_local_timezone(Montreal)
                    .unwrap()
                    .timestamp(); // Local TZ Montreal to Utc timestamp

                // Calculate totp
                let mut totp = 0;
                let mut buffer = [0u8; 32];
                if let Ok(secret) = b32decode(&t_sk_l.data[(count - 1) as usize].key, &mut buffer) {
                    totp =
                        totp_custom::<Sha1>(DEFAULT_STEP, 6, secret, unix_timestamp as u64) as u32;
                } // If decode error totp = 0

                TOTP.store(totp, Ordering::Release);

                let totp = leftpad_u32_to_string::<6>(totp, '0'); // Format totp to string

                // Display totp
                print_totp(
                    mtx_deref_mut!(display),
                    title,
                    &totp,
                    native_date_time.second(),
                );
            }
        }
        ticker.next().await;
    }
}
