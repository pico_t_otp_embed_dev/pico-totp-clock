use defmt::*;

use core::cell::RefCell;
use core::ops::DerefMut;
use core::sync::atomic::{AtomicBool, Ordering};

use embassy_embedded_hal::shared_bus::blocking::spi::SpiDevice;
use embassy_sync::blocking_mutex::{raw::NoopRawMutex, NoopMutex};
use embassy_time::{Duration, Timer};

use embassy_rp::gpio;
use embassy_rp::peripherals::{DMA_CH1, DMA_CH2, PIN_4, PIN_5, PIN_6, PIN_7, SPI0};
use embassy_rp::spi::{Async, Config as SpiConfig, Spi};

// NFC Spi
use mfrc522::comm::blocking::spi::{DummyDelay, SpiInterface};
use mfrc522::error::Error;
use mfrc522::{Initialized, Mfrc522};

use gpio::{Level, Output};
use static_cell::make_static;

// My own library
use ee24x08::{self as EE};

// My own module
use crate::rotenc::COUNT;
use crate::totp_clock::read_totp_from_eeprom;
use crate::ws2812::{get_rgb_led, paint_rgb_led, LED_COLOR};
use crate::{
    get_nfc_tag_id, init_cc20_key, init_nfc_tag_id, mtx_deref_mut, EeMtx, TotpSecretKey, CC20_KEY,
    EE_CFG_ADD, NFC_TAG_ID, TOTP_SK_LIST,
};

// NFC
pub type MC522 = Mfrc522<
    SpiInterface<
        SpiDevice<'static, NoopRawMutex, Spi<'static, SPI0, Async>, Output<'static>>,
        DummyDelay,
    >,
    Initialized,
>;

pub static LOCK: AtomicBool = AtomicBool::new(true); // LOCK state, if NFC Tag is unavailable = true

fn print_nfc_err<E>(err: &Error<E>) {
    match err {
        Error::Bcc => error!("NFC: error BCC"),
        Error::BufferOverflow => error!("NFC: error BufferOverflow"),
        Error::Collision => error!("NFC: error Collision"),
        Error::Crc => error!("NFC: error Crc"),
        Error::IncompleteFrame => error!("NFC: error Incomplete"),
        Error::NoRoom => error!("NFC: error NoRoom"),
        Error::Overheating => error!("NFC: error Overheating"),
        Error::Parity => error!("NFC: error Parity"),
        Error::Protocol => error!("NFC: error Protocol"),
        Error::Timeout => error!("NFC: error Timeout"),
        Error::Wr => error!("NFC: error Wr"),
        Error::Nak => error!("NFC: error Nak"),
        _ => error!("NFC: error SPI"),
    }
}

// Function to compare two arrays of fixed size 7
fn validate_uid(arr1: &[u8; 7], arr2: &[u8]) -> bool {
    // Iterate over each element of both arrays
    for (a, b) in arr1.iter().zip(arr2.iter()) {
        // If any element is not equal, return false
        if a != b {
            return false;
        }
    }
    // If all elements are equal, return true
    true
}

// Init one time the totp secret key list from eeprom
pub async fn init_totp_sk_list(max_count: u32, mtx_ee: &'static EeMtx) {
    // Read and decrypt each totp from eeprom
    let mut totp_sk_list = TOTP_SK_LIST.lock().await;
    if !totp_sk_list.initialised {
        for i in 0..max_count {
            let totp_sk = read_totp_from_eeprom(i + 1, mtx_ee).await;
            totp_sk_list
                .data
                .push(TotpSecretKey {
                    title: totp_sk.title,
                    key: totp_sk.key,
                })
                .unwrap();
        }
        totp_sk_list.initialised = true;
    }
}

pub fn create_nfc_device(
    spi: SPI0,
    cs: PIN_5,
    clk: PIN_6,
    mosi: PIN_7,
    miso: PIN_4,
    tx_dma: DMA_CH1,
    rx_dma: DMA_CH2,
) -> &'static mut MC522 {
    // create SPI config
    let mut nfc_config = SpiConfig::default();
    nfc_config.frequency = 1_000_000;

    let spi = Spi::new(spi, clk, mosi, miso, tx_dma, rx_dma, nfc_config);
    let spi_bus = make_static!(NoopMutex::new(RefCell::new(spi)));
    let nfc_spi = SpiDevice::new(spi_bus, Output::new(cs, Level::High));
    let itf = SpiInterface::new(nfc_spi);
    let mfrc522 = make_static!(Mfrc522::new(itf).init().unwrap());

    mfrc522
}

#[embassy_executor::task]
pub async fn nfc_task(nfc: &'static mut MC522, mtx_ee: &'static EeMtx, max_count: u32) {
    loop {
        nfc_handle_card(nfc, mtx_ee, max_count).await;

        // Manage LED color
        if !LOCK.load(Ordering::Relaxed) {
            // If Unlocked change color to green only if no NTP error
            let rgb_led = get_rgb_led().await;
            if let Some(nb_blink) = rgb_led.nb_blink {
                if nb_blink != 2 {
                    paint_rgb_led(LED_COLOR::GREEN).await;
                }
            }
        } else {
            // If locked change color to ORANGE only if no NTP error
            let rgb_led = get_rgb_led().await;
            if let Some(nb_blink) = rgb_led.nb_blink {
                if nb_blink != 2 {
                    paint_rgb_led(LED_COLOR::ORANGE).await;
                }
            }
        }
    }
}

pub async fn nfc_handle_card(nfc: &mut MC522, mtx_ee: &'static EeMtx, max_count: u32) {
    // Local Fn to lock and set count to 0 and clear CC20_KEY and TOTP_SK_LIST
    let lock_and_clear = || {
        LOCK.store(true, Ordering::Relaxed);
        COUNT.store(0, Ordering::Relaxed);
        async {
            let mut totp_sk_list = TOTP_SK_LIST.lock().await;
            let mut cc20_key = CC20_KEY.lock().await;
            totp_sk_list.data.clear();
            totp_sk_list.initialised = false;
            cc20_key.key = [0; 32];
            cc20_key.initialised = false;
        }
    };

    // Initialize NFC Tag ID From eeprom
    if !NFC_TAG_ID.lock().await.initialised {
        let mut ee_tag_id: [u8; 7] = [0; 7];
        EE::read_buf(mtx_deref_mut!(mtx_ee), &mut ee_tag_id, EE_CFG_ADD::TAGID).await;
        init_nfc_tag_id(ee_tag_id).await;
    }

    // Test if nfc tag is available
    let _ = nfc.new_card_present();
    Timer::after(Duration::from_millis(250)).await;
    if let Ok(atqa) = nfc.reqa() {
        if let Ok(ntid) = nfc.select(&atqa) {
            if validate_uid(&get_nfc_tag_id().await, ntid.as_bytes()) {
                // Read cc20_key from nfc tag
                if !CC20_KEY.lock().await.initialised {
                    info!("NFC: Valid tag id !!!");
                    let mut rx_buffer = [0u8; 32];
                    let mut read_error = false;

                    match nfc.mf_read(0x10) {
                        Ok(data) => {
                            rx_buffer[..16].copy_from_slice(&data);
                        }
                        Err(err) => {
                            error!("NFC: Error during first 16 bytes read from nfc tag");
                            print_nfc_err(&err);
                            read_error = true;
                        }
                    }

                    match nfc.mf_read(0x14) {
                        Ok(data) => {
                            rx_buffer[16..(16 + 16)].copy_from_slice(&data);
                        }
                        Err(err) => {
                            error!("NFC: Error during last 16 bytes read from nfc tag");
                            print_nfc_err(&err);
                            read_error = true;
                        }
                    }

                    if !read_error {
                        init_cc20_key(rx_buffer).await;
                        info!("NFC: cc20_key initialised !!!");
                        if !TOTP_SK_LIST.lock().await.initialised {
                            init_totp_sk_list(max_count, mtx_ee).await;
                            info!("NFC: totp_sk_list initialised !!!");
                        }
                    } else {
                        error!("NFC: Bad NFC tag !!!");
                        lock_and_clear().await;
                    }
                }
                LOCK.store(false, Ordering::Relaxed);
            } else if !LOCK.load(Ordering::Relaxed) {
                error!("NFC: Bad NFC tag !!!");
                lock_and_clear().await;
            }
        }
    } else if !LOCK.load(Ordering::Relaxed) {
        error!("NFC: No card detected !!!");
        lock_and_clear().await;
    }
}
